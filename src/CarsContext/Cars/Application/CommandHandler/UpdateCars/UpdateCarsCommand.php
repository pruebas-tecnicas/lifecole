<?php

declare(strict_types=1);

namespace Lifecole\Main\CarsContext\Cars\Application\CommandHandler\UpdateCars;

use Lifecole\Main\SharedContext\Application\Message;

final class UpdateCarsCommand extends Message
{
    private ?string $filePath;

    private function __construct(?string $filePath)
    {
        $this->filePath = $filePath;
    }

    public static function create(?string $filePath): self
    {
        return new static($filePath);
    }

    public function filePath(): ?string
    {
        return $this->filePath;
    }
}
