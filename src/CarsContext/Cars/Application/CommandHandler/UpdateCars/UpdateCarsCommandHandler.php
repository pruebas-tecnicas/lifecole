<?php

declare(strict_types=1);

namespace Lifecole\Main\CarsContext\Cars\Application\CommandHandler\UpdateCars;

use DateTimeImmutable;
use Lifecole\Main\CarsContext\Cars\Domain\Entity\CarImporterInterface;
use Lifecole\Main\CarsContext\Cars\Domain\Entity\CarReadRepositoryInterface;
use Lifecole\Main\CarsContext\Cars\Domain\Entity\CarWriteRepositoryInterface;
use Lifecole\Main\CarsContext\Cars\Domain\ValueObject\Enabled;
use Lifecole\Main\SharedContext\Application\CommandBus\CommandHandler;
use Lifecole\Main\SharedContext\Application\Message;

class UpdateCarsCommandHandler extends CommandHandler
{
    private CarImporterInterface        $carImporter;
    private CarReadRepositoryInterface  $carReadRepository;
    private CarWriteRepositoryInterface $carWriteRepository;

    public function __construct(
        CarImporterInterface $carImporter,
        CarReadRepositoryInterface $carReadRepository,
        CarWriteRepositoryInterface $carWriteRepository
    ) {
        $this->carImporter        = $carImporter;
        $this->carReadRepository  = $carReadRepository;
        $this->carWriteRepository = $carWriteRepository;
    }

    /**
     * @param Message|UpdateCarsCommand $command
     */
    public function __invoke(Message $command): void
    {
        if (!is_null($command->filePath())) {
            $this->carImporter->initialize($command->filePath());
        }

        $updateStartAt = new DateTimeImmutable();

        while ($this->carImporter->areThereMoreCars()) {
            $car = $this->carImporter->getNextCar();
            $this->carWriteRepository->upsert($car);
        }

        while ($car = $this->carReadRepository->findOneCarEnabledAndOlderThan($updateStartAt)) {
            $car->updateEnabled(Enabled::disabled());
            $this->carWriteRepository->upsert($car);
        }
    }
}
