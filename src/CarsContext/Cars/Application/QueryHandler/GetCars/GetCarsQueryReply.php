<?php

declare(strict_types=1);

namespace Lifecole\Main\CarsContext\Cars\Application\QueryHandler\GetCars;

use Lifecole\Main\CarsContext\Cars\Domain\Entity\Cars;
use Lifecole\Main\CarsContext\Cars\Infrastructure\Repository\CarConverter;
use Lifecole\Main\SharedContext\Application\Reply;

class GetCarsQueryReply extends Reply
{
    private Cars $cars;

    public function __construct(Cars $cars)
    {
        $this->cars = $cars;
    }

    public static function create(Cars $cars): self
    {
        return new static($cars);
    }

    public function cars(): array
    {
        $cars = [];
        foreach ($this->cars as $car) {
            $cars[] = CarConverter::carToArray($car, '');
        }

        return $cars;
    }
}
