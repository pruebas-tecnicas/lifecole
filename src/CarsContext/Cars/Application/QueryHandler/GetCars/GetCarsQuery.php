<?php

declare(strict_types=1);

namespace Lifecole\Main\CarsContext\Cars\Application\QueryHandler\GetCars;

use Lifecole\Main\SharedContext\Application\Message;
use Lifecole\Main\SharedContext\Domain\Exception\InvalidParameterException;

final class GetCarsQuery extends Message
{
    const BRAND        = 'brand';
    const MODEL        = 'model';
    const PRICE        = 'price';
    const VALID_FIELDS = [self::BRAND, self::MODEL, self::PRICE];

    private ?string $filterField;
    private $filterValue;
    private ?string $orderField;
    private string $customerId;
    private ?bool   $favourite;
    private ?int     $page;

    public function __construct(
        ?string $filterField,
        $filterValue,
        ?string $orderField,
        string $customerId,
        ?bool $favourite,
        ?int $page
    ) {
        $this->filterField = $filterField;
        $this->filterValue = $filterValue;
        $this->orderField  = $orderField;
        $this->customerId  = $customerId;
        $this->favourite   = $favourite;
        $this->page        = $page;
    }

    /**
     * @param string|null $filterField
     * @param             $filterValue
     * @param string|null $orderField
     * @param string      $customerId
     * @param bool|null   $favourite
     * @param int|null    $page
     *
     * @return static
     * @throws InvalidParameterException
     */
    public static function create(
        ?string $filterField,
        $filterValue,
        ?string $orderField,
        string $customerId,
        ?bool $favourite,
        ?int $page
    ): self {
        if (!is_null($filterField) && !in_array($filterField, self::VALID_FIELDS)) {
            throw InvalidParameterException::create(
                'filter_field',
                $filterField ?? 'null',
                '[' . implode(', ', self::VALID_FIELDS) . ']'
            );
        }

        if ($filterField === self::PRICE && !is_numeric($filterValue)) {
            throw InvalidParameterException::create(
                'filter_value',
                $filterValue ?? 'null',
                'integer number'
            );
        }

        if (!is_null($filterField) && is_null($filterValue)) {
            throw InvalidParameterException::create(
                'filter_value',
                $filterField ?? 'null'
            );
        }

        if (!is_null($orderField) && !in_array($orderField, self::VALID_FIELDS)) {
            throw InvalidParameterException::create(
                'order_field',
                $orderField ?? 'null',
                '[' . implode(', ', self::VALID_FIELDS) . ']'
            );
        }

        return new static($filterField, $filterValue, $orderField, $customerId, $favourite, $page);
    }

    public function filterField(): ?string
    {
        return $this->filterField;
    }

    public function filterValue()
    {
        return $this->filterValue;
    }

    public function orderField(): ?string
    {
        return $this->orderField;
    }

    public function customerId(): string
    {
        return $this->customerId;
    }

    public function favourite(): ?bool
    {
        return $this->favourite;
    }

    public function page(): ?int
    {
        return $this->page;
    }
}
