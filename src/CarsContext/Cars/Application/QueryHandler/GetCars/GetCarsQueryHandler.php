<?php

declare(strict_types=1);

namespace Lifecole\Main\CarsContext\Cars\Application\QueryHandler\GetCars;

use Lifecole\Main\CarsContext\Cars\Domain\Entity\CarReadRepositoryInterface;
use Lifecole\Main\SharedContext\Application\Message;
use Lifecole\Main\SharedContext\Application\QueryBus\QueryHandler;
use Lifecole\Main\SharedContext\Domain\Exception\CarNotFoundException;

class GetCarsQueryHandler extends QueryHandler
{
    private CarReadRepositoryInterface $carReadRepository;

    public function __construct(CarReadRepositoryInterface $carReadRepository)
    {
        $this->carReadRepository = $carReadRepository;
    }

    /**
     * @param Message|GetCarsQuery $query
     *
     * @return GetCarsQueryReply
     */
    public function __invoke(Message $query): GetCarsQueryReply
    {
        $filter = [];
        if (!is_null($query->filterField())) {
            $filter[$query->filterField()] = $query->filterValue();
        }
        if (!is_null($query->favourite())) {
            $filter['favourite'] = $query->favourite();
            $filter['customer_id'] = $query->customerId();
        }

        $sort = $query->orderField();
        $page = $query->page();

        $cars = $this->carReadRepository->findAll($filter, $sort, $page);


        return GetCarsQueryReply::create($cars);
    }
}
