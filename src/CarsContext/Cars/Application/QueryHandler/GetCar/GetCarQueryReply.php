<?php

declare(strict_types=1);

namespace Lifecole\Main\CarsContext\Cars\Application\QueryHandler\GetCar;

use Lifecole\Main\CarsContext\Cars\Domain\Entity\Car;
use Lifecole\Main\CarsContext\Cars\Domain\ValueObject\Enabled;
use Lifecole\Main\CarsContext\Cars\Domain\ValueObject\ExternalId;
use Lifecole\Main\CarsContext\Cars\Domain\ValueObject\FullModelName;
use Lifecole\Main\CarsContext\Cars\Domain\ValueObject\Price;
use Lifecole\Main\CarsContext\Cars\Domain\ValueObject\Schema;
use Lifecole\Main\CarsContext\Cars\Domain\ValueObject\ShortModelName;
use Lifecole\Main\CarsContext\Cars\Domain\ValueObject\UpdatedAt;
use Lifecole\Main\SharedContext\Application\Reply;

class GetCarQueryReply extends Reply
{
    private Car $car;

    public function __construct(Car $car)
    {
        $this->car = $car;
    }

    public static function create(Car $car): self
    {
        return new static($car);
    }

    public function id(): string
    {
        return $this->car->id()->value();
    }

    public function schema(): string
    {
        return $this->car->schema()->value();
    }

    public function externalId(): string
    {
        return $this->car->externalId()->value();
    }

    public function shortModelName(): string
    {
        return $this->car->shortModelName()->value();
    }

    public function fullModelName(): string
    {
        return $this->car->fullModelName()->value();
    }

    public function price1(): string
    {
        return $this->car->price1()->__toString();
    }

    public function price2(): string
    {
        return $this->car->price2()->__toString();
    }

    public function enabled(): bool
    {
        return $this->car->enabled()->value();
    }

    public function updatedAt(): string
    {
        return $this->car->updatedAt()->value()->format('Y-m-d H:i:s.u O');
    }
}
