<?php

declare(strict_types=1);

namespace Lifecole\Main\CarsContext\Cars\Application\QueryHandler\GetCar;

use Lifecole\Main\SharedContext\Application\Message;
use Lifecole\Main\SharedContext\Domain\Exception\InvalidArgumentException;
use Lifecole\Main\SharedContext\Domain\Identifier\CarId;

final class GetCarQuery extends Message
{
    private CarId $id;

    public function __construct(CarId $id)
    {
        $this->id = $id;
    }

    /**
     * @param string $identifier
     *
     * @return static
     * @throws InvalidArgumentException
     */
    public static function create(string $identifier): self
    {
        return new static(CarId::create($identifier));
    }

    public function id(): CarId
    {
        return $this->id;
    }
}
