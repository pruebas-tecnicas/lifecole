<?php

declare(strict_types=1);

namespace Lifecole\Main\CarsContext\Cars\Application\QueryHandler\GetCar;

use Lifecole\Main\CarsContext\Cars\Domain\Entity\CarReadRepositoryInterface;
use Lifecole\Main\SharedContext\Application\Message;
use Lifecole\Main\SharedContext\Application\QueryBus\QueryHandler;
use Lifecole\Main\SharedContext\Domain\Exception\CarNotFoundException;

class GetCarQueryHandler extends QueryHandler
{
    private CarReadRepositoryInterface $carReadRepository;

    public function __construct(CarReadRepositoryInterface $carReadRepository)
    {
        $this->carReadRepository = $carReadRepository;
    }

    /**
     * @param Message|GetCarQuery $query
     *
     * @return GetCarQueryReply
     * @throws CarNotFoundException
     */
    public function __invoke(Message $query): GetCarQueryReply
    {
        $car = $this->carReadRepository->findById($query->id());
        if (is_null($car)) {
            throw CarNotFoundException::byId($query->id());
        }

        return GetCarQueryReply::create($car);
    }
}
