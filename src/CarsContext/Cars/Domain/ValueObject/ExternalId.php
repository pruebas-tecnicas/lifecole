<?php

declare(strict_types=1);

namespace Lifecole\Main\CarsContext\Cars\Domain\ValueObject;

use Lifecole\Main\SharedContext\Domain\ValueObject\StringValueObject;

class ExternalId extends StringValueObject
{
    public function isAValidValue(string $value): bool
    {
        return preg_match('/^[A-Z]{2}:[A-Z0-9\-]{2,3}$/', $value) === 1;
    }
}
