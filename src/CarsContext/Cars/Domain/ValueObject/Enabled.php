<?php

declare(strict_types=1);

namespace Lifecole\Main\CarsContext\Cars\Domain\ValueObject;

final class Enabled
{
    private bool $value;

    private function __construct(bool $value)
    {
        $this->value = $value;
    }

    public static function create(bool $value): self
    {
        return new static($value);
    }

    public static function enabled(): self
    {
        return new static(true);
    }

    public static function disabled(): self
    {
        return new static(false);
    }

    public function value(): bool
    {
        return $this->value;
    }

    public function equals(self $other): bool
    {
        return $this->value() === $other->value();
    }
}
