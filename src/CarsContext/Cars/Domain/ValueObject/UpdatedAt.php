<?php

declare(strict_types=1);

namespace Lifecole\Main\CarsContext\Cars\Domain\ValueObject;

use DateTimeImmutable;

final class UpdatedAt
{
    private DateTimeImmutable $value;

    private function __construct(DateTimeImmutable $value)
    {
        $this->value = $value;
    }

    public static function create(DateTimeImmutable $value): self
    {
        return new static($value);
    }

    public static function new(): self
    {
        return new static(new DateTimeImmutable());
    }

    public function value(): DateTimeImmutable
    {
        return $this->value;
    }

    public function equals(self $other): bool
    {
        return $this->value()->format(DATE_RFC3339_EXTENDED) === $other->value()->format(DATE_RFC3339_EXTENDED);
    }
}
