<?php

declare(strict_types=1);

namespace Lifecole\Main\CarsContext\Cars\Domain\ValueObject;

use Lifecole\Main\SharedContext\Domain\Exception\InvalidArgumentException;

final class Currency
{
    private string $value;

    public static function create(string $value): self
    {
        return new static($value);
    }

    public static function eur(): self
    {
        return new static('EUR');
    }

    public static function usd(): self
    {
        return new static('USD');
    }

    public function __construct(string $value)
    {
        if (!in_array($value, ['EUR', 'USD'])) {
            throw InvalidArgumentException::create(__METHOD__, $value);
        }

        $this->value = $value;
    }

    public function value(): string
    {
        return $this->value;
    }

    public function equals(self $other): bool
    {
        return $this->value() === $other->value();
    }
}
