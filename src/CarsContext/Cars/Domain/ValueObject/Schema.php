<?php

declare(strict_types=1);

namespace Lifecole\Main\CarsContext\Cars\Domain\ValueObject;

use Lifecole\Main\SharedContext\Domain\ValueObject\StringValueObject;

class Schema extends StringValueObject
{
    public function isAValidValue(string $value): bool
    {
        return preg_match('/^\d{6}$/', $value) === 1;
    }
}
