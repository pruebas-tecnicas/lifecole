<?php

declare(strict_types=1);

namespace Lifecole\Main\CarsContext\Cars\Domain\ValueObject;

use Lifecole\Main\SharedContext\Domain\ValueObject\StringValueObject;

class ShortModelName extends StringValueObject
{
    public function isAValidValue(string $value): bool
    {
        return 1 <= mb_strlen($value) && mb_strlen($value) <= 28;
    }
}
