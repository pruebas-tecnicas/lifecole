<?php

declare(strict_types=1);

namespace Lifecole\Main\CarsContext\Cars\Domain\ValueObject;

class Price
{
    private Amount $amount;
    private Currency $currency;

    final public static function create(Amount $amount, Currency $currency): self
    {
        return new static($amount, $currency);
    }

    final protected function __construct(Amount $amount, Currency $currency)
    {
        $this->amount = $amount;
        $this->currency = $currency;
    }

    final public function amount(): Amount
    {
        return $this->amount;
    }

    final public function currency(): Currency
    {
        return $this->currency;
    }

    public function equals(self $other): bool
    {
        return $this->amount()->equals($other->amount()) && $this->currency()->equals($other->currency());
    }

    public function __toString()
    {
        return sprintf('%01.2F %s', $this->amount()->value(), $this->currency()->value());
    }
}
