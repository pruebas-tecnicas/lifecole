<?php
declare(strict_types=1);

namespace Lifecole\Main\CarsContext\Cars\Domain\Entity;

interface CarImporterInterface
{
    public function initialize(string $filename): void;

    public function areThereMoreCars(): bool;

    public function getNextCar(): Car;
}
