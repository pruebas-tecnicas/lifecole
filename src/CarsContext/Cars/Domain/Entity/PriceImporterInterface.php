<?php

declare(strict_types=1);

namespace Lifecole\Main\CarsContext\Cars\Domain\Entity;

use Lifecole\Main\CarsContext\Cars\Domain\ValueObject\Currency;
use Lifecole\Main\CarsContext\Cars\Domain\ValueObject\ExternalId;
use Lifecole\Main\CarsContext\Cars\Domain\ValueObject\Price;

interface PriceImporterInterface
{
    public function findByExternalIdAndCurrency(ExternalId $externalId, Currency $currency): Price;
}
