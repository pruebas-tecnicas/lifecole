<?php

declare(strict_types=1);

namespace Lifecole\Main\CarsContext\Cars\Domain\Entity;

interface CarWriteRepositoryInterface
{
    public function upsert(Car $car): void;

    public function delete(Car $car): void;
}
