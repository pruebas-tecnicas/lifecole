<?php

declare(strict_types=1);

namespace Lifecole\Main\CarsContext\Cars\Domain\Entity;

use DateTimeImmutable;
use Lifecole\Main\CarsContext\Cars\Domain\ValueObject\ExternalId;
use Lifecole\Main\SharedContext\Domain\Identifier\CarId;

interface CarReadRepositoryInterface
{
    public function findById(CarId $id): ?Car;

    public function findByExternalId(ExternalId $externalId): ?Car;

    public function findOneCarEnabledAndOlderThan(DateTimeImmutable $dateTime): ?Car;

    public function findAll(array $filters, ?string $sort, ?int $page): Cars;
}
