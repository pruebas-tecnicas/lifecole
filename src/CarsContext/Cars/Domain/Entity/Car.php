<?php

declare(strict_types=1);

namespace Lifecole\Main\CarsContext\Cars\Domain\Entity;

use Lifecole\Main\CarsContext\Cars\Domain\Event\CarCreatedEvent;
use Lifecole\Main\CarsContext\Cars\Domain\Event\CarFieldUpdatedEvent;
use Lifecole\Main\CarsContext\Cars\Domain\ValueObject\Enabled;
use Lifecole\Main\CarsContext\Cars\Domain\ValueObject\ExternalId;
use Lifecole\Main\CarsContext\Cars\Domain\ValueObject\FullModelName;
use Lifecole\Main\CarsContext\Cars\Domain\ValueObject\Price;
use Lifecole\Main\CarsContext\Cars\Domain\ValueObject\Schema;
use Lifecole\Main\CarsContext\Cars\Domain\ValueObject\ShortModelName;
use Lifecole\Main\CarsContext\Cars\Domain\ValueObject\UpdatedAt;
use Lifecole\Main\SharedContext\Domain\Entity\AggregateRoot;
use Lifecole\Main\SharedContext\Domain\Identifier\CarId;
use Lifecole\Main\SharedContext\Domain\Identifier\Identifier;

class Car extends AggregateRoot
{
    private Schema $schema;
    private ExternalId $externalId;
    private ShortModelName $shortModelName;
    private FullModelName $fullModelName;
    private Price $price1;
    private Price $price2;
    private Enabled $enabled;
    private UpdatedAt $updatedAt;

    final public static function create(
        CarId $id,
        Schema $schema,
        ExternalId $externalId,
        ShortModelName $shortModelName,
        FullModelName $fullModelName,
        Price $price1,
        Price $price2,
        Enabled $enabled,
        UpdatedAt $updatedAt
    ): self {
        return new static(
            $id,
            $schema,
            $externalId,
            $shortModelName,
            $fullModelName,
            $price1,
            $price2,
            $enabled,
            $updatedAt
        );
    }

    final protected function __construct(
        Identifier $id,
        Schema $schema,
        ExternalId $externalId,
        ShortModelName $shortModelName,
        FullModelName $fullModelName,
        Price $price1,
        Price $price2,
        Enabled $enabled,
        UpdatedAt $updatedAt
    ) {
        parent::__construct($id);

        $this->schema         = $schema;
        $this->externalId     = $externalId;
        $this->shortModelName = $shortModelName;
        $this->fullModelName  = $fullModelName;
        $this->price1         = $price1;
        $this->price2         = $price2;
        $this->enabled        = $enabled;
        $this->updatedAt      = $updatedAt;

        $this->recordEvent(CarCreatedEvent::create($this));
    }

    public function schema(): Schema
    {
        return $this->schema;
    }

    public function externalId(): ExternalId
    {
        return $this->externalId;
    }

    public function shortModelName(): ShortModelName
    {
        return $this->shortModelName;
    }

    public function fullModelName(): FullModelName
    {
        return $this->fullModelName;
    }

    public function price1(): Price
    {
        return $this->price1;
    }

    public function price2(): Price
    {
        return $this->price2;
    }

    public function enabled(): Enabled
    {
        return $this->enabled;
    }

    public function updatedAt(): UpdatedAt
    {
        return $this->updatedAt;
    }

    public function updateSchema(Schema $schema): void
    {
        if ($this->schema->equals($schema)) {
            return;
        }
        $this->updatedAt = UpdatedAt::new();

        $this->recordEvent(
            CarFieldUpdatedEvent::create(
                $this,
                'schema',
                $this->schema->value(),
                $schema->value(),
                $this->updatedAt
            )
        );

        $this->schema = $schema;
    }

    public function updateSortModelName(ShortModelName $shortModelName): void
    {
        if ($this->shortModelName->equals($shortModelName)) {
            return;
        }
        $this->updatedAt = UpdatedAt::new();

        $this->recordEvent(
            CarFieldUpdatedEvent::create(
                $this,
                'short_model_name',
                $this->shortModelName->value(),
                $shortModelName->value(),
                $this->updatedAt
            )
        );

        $this->shortModelName = $shortModelName;
    }

    public function updateFullModelName(FullModelName $fullModelName): void
    {
        if ($this->fullModelName->equals($fullModelName)) {
            return;
        }
        $this->updatedAt = UpdatedAt::new();

        $this->recordEvent(
            CarFieldUpdatedEvent::create(
                $this,
                'full_model_name',
                $this->fullModelName->value(),
                $fullModelName->value(),
                $this->updatedAt
            )
        );

        $this->fullModelName = $fullModelName;
    }

    public function updatePrice1(Price $price): void
    {
        if ($this->price1->equals($price)) {
            return;
        }
        $this->updatedAt = UpdatedAt::new();

        $this->recordEvent(
            CarFieldUpdatedEvent::create(
                $this,
                'price1',
                $this->price1->__toString(),
                $price->__toString(),
                $this->updatedAt
            )
        );

        $this->price1 = $price;
    }

    public function updatePrice2(Price $price): void
    {
        if ($this->price2->equals($price)) {
            return;
        }
        $this->updatedAt = UpdatedAt::new();

        $this->recordEvent(
            CarFieldUpdatedEvent::create(
                $this,
                'price2',
                $this->price2->__toString(),
                $price->__toString(),
                $this->updatedAt
            )
        );

        $this->price2 = $price;
    }

    public function updateEnabled(Enabled $enabled): void
    {
        if ($this->enabled->equals($enabled)) {
            return;
        }
        $this->updatedAt = UpdatedAt::new();

        $this->recordEvent(
            CarFieldUpdatedEvent::create(
                $this,
                'enabled',
                $this->enabled->value(),
                $enabled->value(),
                $this->updatedAt
            )
        );

        $this->enabled = $enabled;
    }

    public function updateUpdatedAt(UpdatedAt $updatedAt): void
    {
        if ($this->updatedAt->equals($updatedAt)) {
            return;
        }

        $this->recordEvent(
            CarFieldUpdatedEvent::create(
                $this,
                'updated_at',
                $this->updatedAt->value()->format('Y-m-d H:i:s.uO'),
                $updatedAt->value()->format('Y-m-d H:i:s.uO'),
                $this->updatedAt
            )
        );

        $this->updatedAt = $updatedAt;
    }
}
