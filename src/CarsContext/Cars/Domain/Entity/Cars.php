<?php

declare(strict_types=1);

namespace Lifecole\Main\CarsContext\Cars\Domain\Entity;

use Lifecole\Main\SharedContext\Domain\Exception\InvalidArgumentException;
use Lifecole\Main\SharedContext\Domain\ValueObject\Collection;

class Cars extends Collection
{
    /**
     * @param Car ...$values
     *
     * @return static
     * @throws InvalidArgumentException
     */
    public static function create(Car ...$values): self
    {
        return new static($values);
    }

    protected function classOfArrayElements(): string
    {
        return Car::class;
    }
}
