<?php

declare(strict_types=1);

namespace Lifecole\Main\CarsContext\Cars\Domain\Event;

use Lifecole\Main\CarsContext\Cars\Domain\Entity\Car;
use Lifecole\Main\CarsContext\Cars\Domain\ValueObject\UpdatedAt;
use Lifecole\Main\SharedContext\Domain\Event\DomainEvent;
use Lifecole\Main\SharedContext\Domain\Exception\InvalidArgumentException;
use Lifecole\Main\SharedContext\Domain\Identifier\CarId;

class CarFieldUpdatedEvent extends DomainEvent
{
    public static function create(Car $car, string $field, $oldValue, $newValue, UpdatedAt $updatedAt): self
    {
        return new static(
            $car->id()->value(),
            [
                'field'      => $field,
                'old_value'  => $oldValue,
                'new_value'  => $newValue,
                'updated_at' => $updatedAt->value()->format(self::EVENT_TIME_FORMAT)
            ],
            1
        );
    }

    /**
     * @return CarId
     * @throws InvalidArgumentException
     */
    public function aggregateRootId(): CarId
    {
        return CarId::create($this->aggregateRootIdString());
    }
}
