<?php

declare(strict_types=1);

namespace Lifecole\Main\CarsContext\Cars\Domain\Event;

use Lifecole\Main\CarsContext\Cars\Domain\Entity\Car;
use Lifecole\Main\SharedContext\Domain\Event\DomainEvent;
use Lifecole\Main\SharedContext\Domain\Exception\InvalidArgumentException;
use Lifecole\Main\SharedContext\Domain\Identifier\CarId;

class CarCreatedEvent extends DomainEvent
{
    public static function create(Car $car): self
    {
        return new static(
            $car->id()->value(),
            [
                'schema'           => $car->schema()->value(),
                'external_id'      => $car->externalId()->value(),
                'short_model_name' => $car->shortModelName()->value(),
                'full_model_name'  => $car->fullModelName()->value()
            ],
            1
        );
    }

    /**
     * @return CarId
     * @throws InvalidArgumentException
     */
    public function aggregateRootId(): CarId
    {
        return CarId::create($this->aggregateRootIdString());
    }
}
