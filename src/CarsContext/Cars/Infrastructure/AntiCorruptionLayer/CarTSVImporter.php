<?php

declare(strict_types=1);

namespace Lifecole\Main\CarsContext\Cars\Infrastructure\AntiCorruptionLayer;

use Iterator;
use Lifecole\Main\CarsContext\Cars\Domain\Entity\Car;
use Lifecole\Main\CarsContext\Cars\Domain\Entity\CarImporterInterface;
use Lifecole\Main\CarsContext\Cars\Domain\Entity\CarReadRepositoryInterface;
use Lifecole\Main\CarsContext\Cars\Domain\Entity\PriceImporterInterface;
use Lifecole\Main\CarsContext\Cars\Domain\ValueObject\Currency;
use Lifecole\Main\CarsContext\Cars\Domain\ValueObject\Enabled;
use Lifecole\Main\CarsContext\Cars\Domain\ValueObject\ExternalId;
use Lifecole\Main\CarsContext\Cars\Domain\ValueObject\FullModelName;
use Lifecole\Main\CarsContext\Cars\Domain\ValueObject\Schema;
use Lifecole\Main\CarsContext\Cars\Domain\ValueObject\ShortModelName;
use Lifecole\Main\CarsContext\Cars\Domain\ValueObject\UpdatedAt;
use Lifecole\Main\SharedContext\Domain\Exception\InvalidArgumentException;
use Lifecole\Main\SharedContext\Domain\Exception\InvalidContentException;
use Lifecole\Main\SharedContext\Domain\Exception\InvalidFileException;
use Lifecole\Main\SharedContext\Domain\Identifier\CarId;

class CarTSVImporter implements CarImporterInterface
{
    const SCHEMA_ID        = 'schema_id';
    const EXTERNAL_ID      = 'data_value';
    const SHORT_MODEL_NAME = 'abbr_text';
    const FULL_MODEL_NAME  = 'full_text';
    const PRICE1_AMOUNT    = 'price1_amount';
    const PRICE1_CURRENCY  = 'price1_currency';
    const PRICE2_AMOUNT    = 'price2_amount';
    const PRICE2_CURRENCY  = 'price2_currency';

    private Iterator                   $iterator;
    private CarReadRepositoryInterface $carReadRepository;
    private PriceImporterInterface     $priceImporter;

    public function __construct(CarReadRepositoryInterface $carReadRepository, PriceImporterInterface $priceImporter)
    {
        $this->carReadRepository = $carReadRepository;
        $this->priceImporter     = $priceImporter;
    }

    /**
     * @param string $filename
     *
     * @throws InvalidContentException
     * @throws InvalidFileException
     */
    public function initialize(string $filename): void
    {
        $this->iterator = new TSVIterator($filename);
    }

    public function areThereMoreCars(): bool
    {
        return $this->iterator->valid();
    }

    /**
     * @return Car
     * @throws InvalidArgumentException
     */
    public function getNextCar(): Car
    {
        $record = $this->iterator->current();
        $this->iterator->next();

        $car = $this->carReadRepository->findByExternalId(ExternalId::create($record[self::EXTERNAL_ID]));

        if (is_null($car)) {
            $externalId = ExternalId::create($record[self::EXTERNAL_ID]);
            $price1     = $this->priceImporter->findByExternalIdAndCurrency($externalId, Currency::eur());
            $price2     = $this->priceImporter->findByExternalIdAndCurrency($externalId, Currency::usd());

            return Car::create(
                CarId::new(),
                Schema::create($record[self::SCHEMA_ID]),
                $externalId,
                ShortModelName::create($record[self::SHORT_MODEL_NAME]),
                FullModelName::create($record[self::FULL_MODEL_NAME]),
                $price1,
                $price2,
                Enabled::enabled(),
                UpdatedAt::new()
            );
        }

        $price1 = $this->priceImporter->findByExternalIdAndCurrency($car->externalId(), Currency::eur());
        $price2 = $this->priceImporter->findByExternalIdAndCurrency($car->externalId(), Currency::usd());

        $car->updateSchema(Schema::create($record[self::SCHEMA_ID]));
        $car->updateSortModelName(ShortModelName::create($record[self::SHORT_MODEL_NAME]));
        $car->updateFullModelName(FullModelName::create($record[self::FULL_MODEL_NAME]));
        $car->updatePrice1($price1);
        $car->updatePrice2($price2);
        $car->updateEnabled(Enabled::enabled());
        $car->updateUpdatedAt(UpdatedAt::new());

        return $car;
    }
}
