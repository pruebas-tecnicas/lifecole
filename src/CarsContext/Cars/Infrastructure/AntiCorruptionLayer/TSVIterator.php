<?php

declare(strict_types=1);

namespace Lifecole\Main\CarsContext\Cars\Infrastructure\AntiCorruptionLayer;

use Iterator;
use Lifecole\Main\SharedContext\Domain\Exception\InvalidContentException;
use Lifecole\Main\SharedContext\Domain\Exception\InvalidFileException;

class TSVIterator implements Iterator
{
    private string $filename;

    /** @var resource $handler */
    private $handler;

    /** @var string[] $titles*/
    private array $titles;

    /** @var string[] $current*/
    private array $current;

    private bool $valid;
    private int $line;

    /**
     * @param string $filename
     *
     * @throws InvalidContentException
     * @throws InvalidFileException
     */
    final public function __construct(string $filename)
    {
        $this->filename = $filename;

        $this->openAndGoToStart();
    }

    public function current()
    {
        return array_combine($this->titles, $this->current);
    }

    /**
     * @throws InvalidContentException
     */
    public function next()
    {
        $this->current = $this->getNextLine();
    }

    public function key()
    {
        return $this->line;
    }

    public function valid()
    {
        return $this->valid;
    }

    /**
     * @throws InvalidContentException
     * @throws InvalidFileException
     */
    public function rewind()
    {
        $this->openAndGoToStart();
    }

    /**
     * @throws InvalidContentException
     * @throws InvalidFileException
     */
    private function openAndGoToStart(): void
    {
        $this->line  = 0;
        $this->valid = false;

        if (is_resource($this->handler)) {
            fclose($this->handler);
            unset($this->handler);
        }

        $this->handler = fopen($this->filename, 'r');
        if (!is_resource($this->handler)) {
            throw InvalidFileException::create($this->filename);
        }

        $this->line    = 0;
        $this->valid   = true;
        $this->titles  = $this->getNextLine();
        $this->current = $this->getNextLine();
    }

    /**
     * @return array
     * @throws InvalidContentException
     */
    private function getNextLine(): array
    {
        $record = fgetcsv($this->handler, 0, "\t", "'", "");

        if (is_null($record) || is_bool($record)) {
            $this->valid = false;
            return [];
        }

        if (!is_array($record) || count($record) !== 4) {
            throw InvalidContentException::create();
        }

        $this->line++;

        return $this->cleanText($record);
    }

    private function cleanText(array $record): array
    {
        return array_map(fn($value): string => trim($value), $record);
    }
}
