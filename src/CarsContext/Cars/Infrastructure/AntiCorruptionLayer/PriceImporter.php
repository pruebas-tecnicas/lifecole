<?php

declare(strict_types=1);

namespace Lifecole\Main\CarsContext\Cars\Infrastructure\AntiCorruptionLayer;

use Lifecole\Main\CarsContext\Cars\Domain\Entity\PriceImporterInterface;
use Lifecole\Main\CarsContext\Cars\Domain\ValueObject\Amount;
use Lifecole\Main\CarsContext\Cars\Domain\ValueObject\Currency;
use Lifecole\Main\CarsContext\Cars\Domain\ValueObject\ExternalId;
use Lifecole\Main\CarsContext\Cars\Domain\ValueObject\Price;

class PriceImporter implements PriceImporterInterface
{
    public function findByExternalIdAndCurrency(ExternalId $externalId, Currency $currency): Price
    {
        $handler = curl_init();
        curl_setopt($handler, CURLOPT_URL, 'http://www.randomnumberapi.com/api/v1.0/random?min=10000&max=20000&count=1');
        curl_setopt($handler, CURLOPT_HEADER, ['Content-Type: application/json']);
        curl_setopt($handler, CURLOPT_RETURNTRANSFER, 1);
        $response = explode("\r\n\r\n", curl_exec($handler));
        $content = json_decode($response[1]);
        return Price::create(
            Amount::create((float)$content[0]),
            $currency
        );
    }
}
