<?php

declare(strict_types=1);

namespace Lifecole\Main\CarsContext\Cars\Infrastructure\Repository;

use DateTimeImmutable;
use Lifecole\Main\CarsContext\Cars\Domain\Entity\Car;
use Lifecole\Main\CarsContext\Cars\Domain\ValueObject\Amount;
use Lifecole\Main\CarsContext\Cars\Domain\ValueObject\Currency;
use Lifecole\Main\CarsContext\Cars\Domain\ValueObject\Enabled;
use Lifecole\Main\CarsContext\Cars\Domain\ValueObject\ExternalId;
use Lifecole\Main\CarsContext\Cars\Domain\ValueObject\FullModelName;
use Lifecole\Main\CarsContext\Cars\Domain\ValueObject\Price;
use Lifecole\Main\CarsContext\Cars\Domain\ValueObject\Schema;
use Lifecole\Main\CarsContext\Cars\Domain\ValueObject\ShortModelName;
use Lifecole\Main\CarsContext\Cars\Domain\ValueObject\UpdatedAt;
use Lifecole\Main\SharedContext\Domain\Exception\InvalidArgumentException;
use Lifecole\Main\SharedContext\Domain\Identifier\CarId;

final class CarConverter
{
    const ID               = 'id';
    const SCHEMA_ID        = 'schema_id';
    const EXTERNAL_ID      = 'external_id';
    const SHORT_MODEL_NAME = 'short_model_name';
    const FULL_MODEL_NAME  = 'full_model_name';
    const UPDATED_AT       = 'updated_at';
    const PRICE_1_AMOUNT   = 'price1_amount';
    const PRICE_1_CURRENCY = 'price1_currency';
    const PRICE_2_AMOUNT   = 'price2_amount';
    const PRICE_2_CURRENCY = 'price2_currency';
    const ENABLED          = 'enabled';

    public static function carToArray(Car $car, string $prefix = ':'): array
    {
        return [
            $prefix . self::ID               => $car->id()->value(),
            $prefix . self::SCHEMA_ID        => $car->schema()->value(),
            $prefix . self::EXTERNAL_ID      => $car->externalId()->value(),
            $prefix . self::SHORT_MODEL_NAME => $car->shortModelName()->value(),
            $prefix . self::FULL_MODEL_NAME  => $car->fullModelName()->value(),
            $prefix . self::PRICE_1_AMOUNT   => $car->price1()->amount()->value(),
            $prefix . self::PRICE_1_CURRENCY => $car->price1()->currency()->value(),
            $prefix . self::PRICE_2_AMOUNT   => $car->price2()->amount()->value(),
            $prefix . self::PRICE_2_CURRENCY => $car->price2()->currency()->value(),
            $prefix . self::ENABLED          => $car->enabled()->value(),
            $prefix . self::UPDATED_AT       => $car->updatedAt()->value()->format('Y-m-d H:i:s.uO')
        ];
    }

    /**
     * @param array $row
     *
     * @return Car
     * @throws InvalidArgumentException
     */
    public static function arrayToCar(array $row): Car
    {
        $car = Car::create(
            CarId::create($row[self::ID]),
            Schema::create($row[self::SCHEMA_ID]),
            ExternalId::create($row[self::EXTERNAL_ID]),
            ShortModelName::create($row[self::SHORT_MODEL_NAME]),
            FullModelName::create($row[self::FULL_MODEL_NAME]),
            Price::create(
                Amount::create(floatval($row[self::PRICE_1_AMOUNT])),
                Currency::create($row[self::PRICE_1_CURRENCY])
            ),
            Price::create(
                Amount::create(floatval($row[self::PRICE_2_AMOUNT])),
                Currency::create($row[self::PRICE_2_CURRENCY])
            ),
            Enabled::create($row[self::ENABLED]),
            UpdatedAt::create(DateTimeImmutable::createFromFormat('Y-m-d H:i:s.uO', $row[self::UPDATED_AT]))
        );

        $car->takeEvents();

        return $car;
    }
}
