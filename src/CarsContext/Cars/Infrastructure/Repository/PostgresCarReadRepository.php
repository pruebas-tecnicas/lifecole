<?php

declare(strict_types=1);

namespace Lifecole\Main\CarsContext\Cars\Infrastructure\Repository;

use DateTimeImmutable;
use Lifecole\Main\CarsContext\Cars\Domain\Entity\Car;
use Lifecole\Main\CarsContext\Cars\Domain\Entity\CarReadRepositoryInterface;
use Lifecole\Main\CarsContext\Cars\Domain\Entity\Cars;
use Lifecole\Main\CarsContext\Cars\Domain\ValueObject\ExternalId;
use Lifecole\Main\SharedContext\Domain\Exception\InvalidArgumentException;
use Lifecole\Main\SharedContext\Domain\Identifier\CarId;
use Lifecole\Main\SharedContext\Infrastructure\Persistence\DatabaseInterface;

class PostgresCarReadRepository implements CarReadRepositoryInterface
{
    private DatabaseInterface $database;

    public function __construct(DatabaseInterface $database)
    {
        $this->database = $database;
    }

    /**
     * @param CarId $id
     *
     * @return Car|null
     * @throws InvalidArgumentException
     */
    public function findById(CarId $id): ?Car
    {
        $query = 'SELECT * FROM car WHERE id = :id';

        $statement = $this->database->query(
            $query,
            [
                ':id' => $id->value()
            ]
        );

        $result =  $statement->fetch();

        if (!$result) {
            return null;
        }

        return CarConverter::arrayToCar($result);
    }

    /**
     * @param ExternalId $externalId
     *
     * @return Car|null
     * @throws InvalidArgumentException
     */
    public function findByExternalId(ExternalId $externalId): ?Car
    {
        $query = 'SELECT * FROM car WHERE external_id = :external_id';

        $statement = $this->database->query(
            $query,
            [
                ':external_id' => $externalId->value()
            ]
        );

        $result =  $statement->fetch();

        if (!$result) {
            return null;
        }

        return CarConverter::arrayToCar($result);
    }

    /**
     * @param DateTimeImmutable $dateTime
     *
     * @return Car|null
     * @throws InvalidArgumentException
     */
    public function findOneCarEnabledAndOlderThan(DateTimeImmutable $dateTime): ?Car
    {
        $query = 'SELECT * FROM car WHERE enabled = \'true\' AND updated_at < :updated_at LIMIT 1';

        $statement = $this->database->query(
            $query,
            [
                ':updated_at' => $dateTime->format('Y-m-d H:i:s.uO')
            ]
        );

        $result =  $statement->fetch();

        if (!$result) {
            return null;
        }

        return CarConverter::arrayToCar($result);
    }

    /**
     * @param array       $filters
     * @param string|null $sort
     * @param int|null    $page
     *
     * @return Cars
     * @throws InvalidArgumentException
     */
    public function findAll(array $filters, ?string $sort, ?int $page): Cars
    {
        $query = 'SELECT c.* FROM car c';
        if (array_key_exists('favourite', $filters) && !is_null($filters['favourite'])) {
            $query .= " INNER JOIN favourite f ON c.id = f.car_id AND f.customer_id = '${filters['customer_id']}'";
        }
        $query .= ' WHERE enabled=true';

        unset($filters['favourite']);
        unset($filters['customer_id']);

        $query = $this->applyFilter($query, $filters);
        $query = $this->applySort($query, $sort);
        $query = $this->applyPagination($query, $page);

        $statement = $this->database->query($query, []);

        $result =  $statement->fetchAll();

        $cars = Cars::create();
        if (!$result) {
            return $cars;
        }

        foreach ($result as $row) {
            $cars[] = CarConverter::arrayToCar($row);
        }

        return $cars;
    }

    private function applyFilter(string $query, array $filters): string
    {
        while (!empty($filters)) {
            $query .= ' AND ';
            $key   = array_key_first($filters);
            $value = array_shift($filters);
            $query .= $this->applyOneFilter($key, $value);
        }

        return $query;
    }

    private function applySort($query, ?string $sort): string
    {
        if ($sort === 'model') {
            return $query . ' ORDER BY c.short_model_name ASC';
        }

        if ($sort === 'brand') {
            return $query . ' ORDER BY c.full_model_name ASC';
        }

        if ($sort === 'price') {
            return $query . ' ORDER BY c.price1_amount ASC';
        }

        return $query;
    }

    private function applyPagination($query, ?int $page): string
    {
        if (is_null($page)) {
            return $query . ' LIMIT 25 OFFSET 0';
        }

        return $query . ' LIMIT 25 OFFSET ' . ($page * 25);
    }

    private function applyOneFilter(string $key, $value): string
    {
        switch ($key) {
            case 'model':
                return "c.short_model_name = '$value'";
            case 'brand':
                return "c.full_model_name LIKE '$value:%'";
            case 'price':
                return "(c.price1_amount < $value OR c.price2_amount < $value)";
        }

        return '';
    }
}
