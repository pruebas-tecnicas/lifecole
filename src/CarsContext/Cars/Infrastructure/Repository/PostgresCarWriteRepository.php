<?php

declare(strict_types=1);

namespace Lifecole\Main\CarsContext\Cars\Infrastructure\Repository;

use Lifecole\Main\CarsContext\Cars\Domain\Entity\Car;
use Lifecole\Main\CarsContext\Cars\Domain\Entity\CarWriteRepositoryInterface;
use Lifecole\Main\SharedContext\Infrastructure\Persistence\DatabaseInterface;

class PostgresCarWriteRepository implements CarWriteRepositoryInterface
{
    private const UPSERT = <<<SQLINSERT
        INSERT INTO
            car(id, schema_id, external_id, short_model_name, full_model_name, price1_amount, price1_currency, price2_amount, price2_currency, enabled, updated_at)
            VALUES(:id, :schema_id, :external_id, :short_model_name, :full_model_name, :price1_amount, :price1_currency, :price2_amount, :price2_currency, :enabled, :updated_at)
            ON CONFLICT (external_id)
            DO UPDATE SET schema_id = :schema_id, short_model_name = :short_model_name, full_model_name = :full_model_name, price1_amount = :price1_amount, price1_currency = :price1_currency, price2_amount = :price2_amount, price2_currency = :price2_currency, enabled = :enabled, updated_at = :updated_at
        ;
SQLINSERT;

    private DatabaseInterface $database;

    public function __construct(DatabaseInterface $database)
    {
        $this->database = $database;
    }

    public function upsert(Car $car): void
    {
        $events = $car->takeEvents();
        if (count($events) === 0) {
            return;
        }

        $this->database->exec(
            self::UPSERT,
            CarConverter::carToArray($car)
        );
    }

    public function delete(Car $car): void
    {
    }
}
