<?php

declare(strict_types=1);

namespace Lifecole\Main\CarsContext\Cars\UserInterface\Controller;

use Lifecole\Main\CarsContext\Cars\Application\QueryHandler\GetCars\GetCarsQuery;
use Lifecole\Main\CarsContext\Cars\Application\QueryHandler\GetCars\GetCarsQueryReply;
use Lifecole\Main\SharedContext\Domain\Exception\InvalidParameterException;
use Lifecole\Main\SharedContext\UserInterface\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class GetCarsController extends Controller
{
    public function __invoke(Request $request)
    {
        try {
            $claims = $this->extractUsernameFromToken($request);
        } catch (Throwable $exception) {
            return new JsonResponse(['error_message' => $exception->getMessage()], Response::HTTP_FORBIDDEN);
        }

        try {
            $query     = GetCarsQuery::create(
                $request->get('filter_field', null),
                $request->get('filter_value', null),
                $request->get('order_field', null),
                $claims['user_id'],
                $this->getFavourite($request),
                $this->getPage($request),
            );
            /** @var GetCarsQueryReply $reply */
            $reply = $this->queryBus->askFor($query);
        } catch (Throwable $exception) {
            return new JsonResponse(['error_message' => str_replace('"', "'", $exception->getMessage())], Response::HTTP_BAD_REQUEST);
        }

        return new JsonResponse($reply->cars(), Response::HTTP_OK, [], false);
    }

    /**
     * @param Request $request
     *
     * @return bool|null
     * @throws InvalidParameterException
     */
    private function getFavourite(Request $request): ?bool
    {
        $value = $request->get('favourite', null);
        if (is_null($value) || $value === 'null') {
            return null;
        }

        if (strtolower($value) === 'true') {
            return true;
        }

        throw InvalidParameterException::create('favourite', $value, '[null, true]');
    }

    /**
     * @param Request $request
     *
     * @return mixed|null
     * @throws InvalidParameterException
     */
    private function getPage(Request $request): ?int
    {
        $value = $request->get('page', null);
        if (is_null($value)) {
            return null;
        }

        if (!is_numeric($value)) {
            throw InvalidParameterException::create('page', $value, '"1, 2, 3, 4, ..."');
        }

        return (int) $value;
    }
}
