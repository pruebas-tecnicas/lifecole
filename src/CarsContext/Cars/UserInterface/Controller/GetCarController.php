<?php

declare(strict_types=1);

namespace Lifecole\Main\CarsContext\Cars\UserInterface\Controller;

use Lifecole\Main\CarsContext\Cars\Application\QueryHandler\GetCar\GetCarQuery;
use Lifecole\Main\CarsContext\Cars\Application\QueryHandler\GetCar\GetCarQueryReply;
use Lifecole\Main\SharedContext\UserInterface\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class GetCarController extends Controller
{
    public function __invoke(Request $request)
    {
        try {
            $this->extractUsernameFromToken($request);
        } catch (Throwable $exception) {
            return new JsonResponse(['error_message' => $exception->getMessage()], Response::HTTP_FORBIDDEN);
        }

        try {
            /** @var GetCarQueryReply $reply */
            $reply = $this->queryBus->askFor(GetCarQuery::create($request->get('identifier')));
        } catch (Throwable $exception) {
            return new JsonResponse(['error_message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }

        $responseData = [
            'id'               => $reply->id(),
            'schema'           => $reply->schema(),
            'external_id'      => $reply->externalId(),
            'short_model_name' => $reply->shortModelName(),
            'full_model_name'  => $reply->fullModelName(),
            'price1'           => $reply->price1(),
            'price2'           => $reply->price2(),
            'updated_at'       => $reply->updatedAt()
        ];
        return new JsonResponse($responseData, Response::HTTP_OK, [], false);
    }
}
