<?php

declare(strict_types=1);

namespace Lifecole\Main\CarsContext\Cars\UserInterface\ConsoleCommand;

use Lifecole\Main\CarsContext\Cars\Domain\Entity\CarImporterInterface;
use Lifecole\Main\CarsContext\Cars\Domain\Entity\CarReadRepositoryInterface;
use Lifecole\Main\SharedContext\UserInterface\ConsoleCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Symfony5CheckCarsImportFileConsoleCommand extends ConsoleCommand
{
    protected static $defaultName = 'demo:cars:checkfile';

    private CarImporterInterface $carImporter;
    private CarReadRepositoryInterface $carReadRepository;

    public function __construct(
        CarImporterInterface $carImporter,
        CarReadRepositoryInterface $carReadRepository
    ) {
        parent::__construct(self::$defaultName);
        $this->carImporter = $carImporter;
        $this->carReadRepository = $carReadRepository;
    }

    protected function configure()
    {
        $this
            ->setDescription('Check a collection of cars from a file')
            ->setHelp('Check a collection of cars from a TSV file (tab separated values) and display some fields')
            ->addArgument('file', InputArgument::OPTIONAL, 'The path to the TSV file')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!is_null($input->getArgument('file'))) {
            $this->carImporter->initialize($input->getArgument('file'));
        }

        while ($this->carImporter->areThereMoreCars()) {
            $car = $this->carImporter->getNextCar();
            $output->write(str_pad($car->schema()->value(), 8, ' ', STR_PAD_RIGHT));
            $output->write(str_pad($car->externalId()->value(), 8, ' ', STR_PAD_RIGHT));
            $output->write(str_pad($car->shortModelName()->value(), 25, ' ', STR_PAD_RIGHT));
            $output->writeln($car->fullModelName()->value());
        }

        return self::SUCCESS;
    }
}
