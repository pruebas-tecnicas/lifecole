<?php

declare(strict_types=1);

namespace Lifecole\Main\CarsContext\Cars\UserInterface\ConsoleCommand;

use Lifecole\Main\CarsContext\Cars\Application\CommandHandler\UpdateCars\UpdateCarsCommand;
use Lifecole\Main\SharedContext\Application\CommandBus\CommandBus;
use Lifecole\Main\SharedContext\UserInterface\ConsoleCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Symfony5ImportCarsConsoleCommand extends ConsoleCommand
{
    protected static $defaultName = 'demo:cars:import';

    private CommandBus $commandBus;

    public function __construct(CommandBus $synchronousCommandBus)
    {
        parent::__construct(self::$defaultName);
        $this->commandBus = $synchronousCommandBus;
    }

    protected function configure()
    {
        $this
            ->setDescription('Import a collection of cars from a file')
            ->setHelp('Import a collection of cars from a TSV file (tab separated values)')
            ->addArgument('file', InputArgument::OPTIONAL, 'The path to the TSV file')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->commandBus->process(UpdateCarsCommand::create($input->getArgument('file')));

        return self::SUCCESS;
    }
}
