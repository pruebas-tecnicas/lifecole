<?php

declare(strict_types=1);

namespace Lifecole\Main\CarsContext\Favorites\Infrastructure\Repository;

use Lifecole\Main\CarsContext\Favorites\Domain\Entity\Favourite;
use Lifecole\Main\CarsContext\Favorites\Domain\Entity\FavouriteWriteRepositoryInterface;

class PostgresFavouriteWriteRepository implements FavouriteWriteRepositoryInterface
{
    public function upsert(Favourite $favourite): void
    {
        // TODO: Implement upsert() method.
    }
}
