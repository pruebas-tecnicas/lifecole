<?php

declare(strict_types=1);

namespace Lifecole\Main\CarsContext\Favorites\Infrastructure\Repository;

use Lifecole\Main\CarsContext\Favorites\Domain\Entity\FavouriteReadRepositoryInterface;

class PostgresFavouriteReadRepository implements FavouriteReadRepositoryInterface
{
}
