<?php

declare(strict_types=1);

namespace Lifecole\Main\CarsContext\Favorites\Domain\Entity;

interface FavouriteWriteRepositoryInterface
{
    public function upsert(Favourite $favourite): void;
}
