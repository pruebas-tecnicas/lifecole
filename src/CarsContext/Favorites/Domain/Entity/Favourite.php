<?php

declare(strict_types=1);

namespace Lifecole\Main\CarsContext\Favorites\Domain\Entity;

use Lifecole\Main\SharedContext\Domain\Entity\Entity;
use Lifecole\Main\SharedContext\Domain\Identifier\CarId;
use Lifecole\Main\SharedContext\Domain\Identifier\FavouriteId;
use Lifecole\Main\SharedContext\Domain\Identifier\UserId;

class Favourite extends Entity
{
    private CarId $carId;
    private UserId $userId;

    public function __construct(FavouriteId $id, CarId $carId, UserId $userId)
    {
        parent::__construct($id);
        $this->carId = $carId;
        $this->userId = $userId;
    }

    public static function create(FavouriteId $id, CarId $carId, UserId $userId): self
    {
        return new static($id, $carId, $userId);
    }

    public function getCarId(): CarId
    {
        return $this->carId;
    }

    public function getUserId(): UserId
    {
        return $this->userId;
    }
}
