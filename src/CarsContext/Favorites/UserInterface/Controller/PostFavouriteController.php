<?php

declare(strict_types=1);

namespace Lifecole\Main\CarsContext\Favorites\UserInterface\Controller;

use Lifecole\Main\SharedContext\UserInterface\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class PostFavouriteController extends Controller
{
    public function __invoke(Request $request): JsonResponse
    {
        try {
            $claims = $this->extractUsernameFromToken($request);
        } catch (Throwable $exception) {
            return new JsonResponse(['error_message' => $exception->getMessage()], Response::HTTP_FORBIDDEN);
        }


        return new JsonResponse($claims);
    }
}
