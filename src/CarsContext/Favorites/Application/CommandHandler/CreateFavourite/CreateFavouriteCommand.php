<?php

declare(strict_types=1);

namespace Lifecole\Main\CarsContext\Favorites\Application\CommandHandler\CreateFavourite;

use Lifecole\Main\SharedContext\Application\Message;
use Lifecole\Main\SharedContext\Domain\Exception\InvalidArgumentException;
use Lifecole\Main\SharedContext\Domain\Identifier\CarId;
use Lifecole\Main\SharedContext\Domain\Identifier\UserId;

class CreateFavouriteCommand extends Message
{
    private UserId $userId;
    private CarId $carId;

    public function __construct(UserId $userId, CarId $carId)
    {
        $this->userId = $userId;
        $this->carId = $carId;
    }

    /**
     * @param string $userId
     * @param string $carId
     *
     * @return static
     * @throws InvalidArgumentException
     */
    public static function create(string $userId, string $carId): self
    {
        return new static(UserId::create($userId), CarId::create($carId));
    }

    public function getUserId(): UserId
    {
        return $this->userId;
    }

    public function getCarId(): CarId
    {
        return $this->carId;
    }
}
