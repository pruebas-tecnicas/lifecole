<?php

declare(strict_types=1);

namespace Lifecole\Main\CarsContext\Favorites\Application\CommandHandler\CreateFavourite;

use Lifecole\Main\CarsContext\Favorites\Domain\Entity\Favourite;
use Lifecole\Main\CarsContext\Favorites\Domain\Entity\FavouriteWriteRepositoryInterface;
use Lifecole\Main\SharedContext\Application\CommandBus\CommandHandler;
use Lifecole\Main\SharedContext\Application\Message;
use Lifecole\Main\SharedContext\Domain\Identifier\FavouriteId;

class CreateFavouriteCommandHandler extends CommandHandler
{
    private FavouriteWriteRepositoryInterface $favouriteWriteRepository;

    public function __construct(FavouriteWriteRepositoryInterface $favouriteWriteRepository)
    {
        $this->favouriteWriteRepository = $favouriteWriteRepository;
    }

    /**
     * @param Message|CreateFavouriteCommand $command
     */
    public function __invoke(Message $command): void
    {
        $favourite = Favourite::create(FavouriteId::new(), $command->getCarId(), $command->getUserId());

        $this->favouriteWriteRepository->upsert($favourite);
    }
}
