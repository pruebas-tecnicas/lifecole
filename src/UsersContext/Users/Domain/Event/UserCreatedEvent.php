<?php

declare(strict_types=1);

namespace Lifecole\Main\UsersContext\Users\Domain\Event;

use Lifecole\Main\SharedContext\Domain\Event\DomainEvent;
use Lifecole\Main\SharedContext\Domain\Exception\InvalidArgumentException;
use Lifecole\Main\SharedContext\Domain\Identifier\UserId;
use Lifecole\Main\UsersContext\Users\Domain\Entity\User;

class UserCreatedEvent extends DomainEvent
{
    public static function create(User $user): self
    {
        return new static(
            $user->id()->value(),
            [
                'username' => $user->username()->value()
            ],
            1
        );
    }

    /**
     * @return UserId
     * @throws InvalidArgumentException
     */
    public function aggregateRootId(): UserId
    {
        return UserId::create($this->aggregateRootIdString());
    }
}
