<?php

declare(strict_types=1);

namespace Lifecole\Main\UsersContext\Users\Domain\Entity;

use Lifecole\Main\SharedContext\Domain\Entity\AggregateRoot;
use Lifecole\Main\SharedContext\Domain\Identifier\Identifier;
use Lifecole\Main\SharedContext\Domain\Identifier\UserId;
use Lifecole\Main\UsersContext\Users\Domain\Event\UserCreatedEvent;
use Lifecole\Main\UsersContext\Users\Domain\ValueObject\Secret;
use Lifecole\Main\UsersContext\Users\Domain\ValueObject\Username;

class User extends AggregateRoot
{
    private Username $username;
    private Secret $secret;

    public static function create(UserId $id, Username $username, Secret $secret): self
    {
        return new static($id, $username, $secret);
    }

    private function __construct(Identifier $id, Username $username, Secret $secret)
    {
        parent::__construct($id);

        $this->username = $username;
        $this->secret   = $secret;

        $this->recordEvent(UserCreatedEvent::create($this));
    }

    public function username(): Username
    {
        return $this->username;
    }

    public function secret(): Secret
    {
        return $this->secret;
    }
}
