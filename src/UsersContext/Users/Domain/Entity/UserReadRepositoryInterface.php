<?php

declare(strict_types=1);

namespace Lifecole\Main\UsersContext\Users\Domain\Entity;

use Lifecole\Main\UsersContext\Users\Domain\ValueObject\Secret;
use Lifecole\Main\UsersContext\Users\Domain\ValueObject\Username;

interface UserReadRepositoryInterface
{
    public function findByUsername(Username $username): ?User;

    public function findByUsernameAndSecret(Username $username, Secret $secret): ?User;
}
