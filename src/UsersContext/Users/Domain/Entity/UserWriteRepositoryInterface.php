<?php

declare(strict_types=1);

namespace Lifecole\Main\UsersContext\Users\Domain\Entity;

interface UserWriteRepositoryInterface
{
    public function save(User $user): void;
}
