<?php

declare(strict_types=1);

namespace Lifecole\Main\UsersContext\Users\Domain\ValueObject;

use Lifecole\Main\SharedContext\Domain\ValueObject\StringValueObject;

class Secret extends StringValueObject
{
    public function isAValidValue(string $value): bool
    {
        return mb_strlen($value) === 32;
    }
}
