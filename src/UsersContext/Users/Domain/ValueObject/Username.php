<?php

declare(strict_types=1);

namespace Lifecole\Main\UsersContext\Users\Domain\ValueObject;

use Lifecole\Main\SharedContext\Domain\ValueObject\StringValueObject;

class Username extends StringValueObject
{
    public function isAValidValue(string $value): bool
    {
        return 2 <= mb_strlen($value) && mb_strlen($value) <= 20;
    }
}
