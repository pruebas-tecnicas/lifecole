<?php

declare(strict_types=1);

namespace Lifecole\Main\UsersContext\Users\Application\QueryHandler\GetUserToken;

use Lifecole\Main\SharedContext\Application\Message;
use Lifecole\Main\SharedContext\Domain\Exception\InvalidArgumentException;
use Lifecole\Main\UsersContext\Users\Domain\ValueObject\Secret;
use Lifecole\Main\UsersContext\Users\Domain\ValueObject\Username;

final class GetUserTokenQuery extends Message
{
    private Username $username;
    private Secret $secret;

    /**
     * @param string $username
     * @param string $password
     *
     * @return static
     * @throws InvalidArgumentException
     */
    public static function create(string $username, string $password): self
    {
        return new static(Username::create($username), Secret::create(md5($password)));
    }

    private function __construct(Username $username, Secret $secret)
    {
        $this->username = $username;
        $this->secret = $secret;
    }

    public function username(): Username
    {
        return $this->username;
    }

    public function secret(): Secret
    {
        return $this->secret;
    }
}
