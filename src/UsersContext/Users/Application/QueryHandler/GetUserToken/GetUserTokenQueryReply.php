<?php

declare(strict_types=1);

namespace Lifecole\Main\UsersContext\Users\Application\QueryHandler\GetUserToken;

use Lcobucci\JWT\Token;
use Lifecole\Main\SharedContext\Application\Reply;

final class GetUserTokenQueryReply extends Reply
{
    private Token $token;

    public static function create(Token $token): self
    {
        return new static($token);
    }

    public function __construct(Token $token)
    {
        $this->token = $token;
    }

    public function getToken(): Token
    {
        return $this->token;
    }
}
