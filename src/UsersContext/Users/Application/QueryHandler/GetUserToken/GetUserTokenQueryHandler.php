<?php

declare(strict_types=1);

namespace Lifecole\Main\UsersContext\Users\Application\QueryHandler\GetUserToken;

use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha512;
use Lcobucci\JWT\Signer\Key;
use Lifecole\Main\SharedContext\Application\Message;
use Lifecole\Main\SharedContext\Application\QueryBus\QueryHandler;
use Lifecole\Main\SharedContext\Application\Reply;
use Lifecole\Main\SharedContext\Domain\Exception\UserNotFoundException;
use Lifecole\Main\UsersContext\Users\Domain\Entity\UserReadRepositoryInterface;

class GetUserTokenQueryHandler extends QueryHandler
{
    private UserReadRepositoryInterface $userReadRepository;

    public function __construct(UserReadRepositoryInterface $userReadRepository)
    {
        $this->userReadRepository = $userReadRepository;
    }

    /**
     * @param Message|GetUserTokenQuery $query
     *
     * @return GetUserTokenQueryReply
     * @throws UserNotFoundException
     */
    public function __invoke(Message $query): GetUserTokenQueryReply
    {
        $user = $this->userReadRepository->findByUsernameAndSecret($query->username(), $query->secret());
        if (is_null($user)) {
            throw UserNotFoundException::create($query->username());
        }

        $time    = time();
        $builder = new Builder();
        $signer  = new Sha512();
        $key     = new Key('secret_pass_frase');
        $token   = $builder
            ->issuedBy('www.demo.wip')
            ->permittedFor('www.demo.wip')
            ->identifiedBy('07091966sz', false)
            ->issuedAt($time)
            ->expiresAt($time + 300)
            ->withClaim('uid', rand(100000, 999999))
            ->withClaim('user_id', $user->id()->value())
            ->withClaim('username', $user->username()->value())
            ->getToken($signer, $key)
        ;

        return GetUserTokenQueryReply::create($token);
    }
}
