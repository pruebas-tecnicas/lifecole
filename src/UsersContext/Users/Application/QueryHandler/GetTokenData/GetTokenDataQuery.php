<?php

declare(strict_types=1);

namespace Lifecole\Main\UsersContext\Users\Application\QueryHandler\GetTokenData;

use Lifecole\Main\SharedContext\Application\Message;

final class GetTokenDataQuery extends Message
{
    private string $token;

    public static function create(string $token): self
    {
        return new static($token);
    }

    private function __construct(string $token)
    {
        $this->token = $token;
    }

    public function token(): string
    {
        return $this->token;
    }
}
