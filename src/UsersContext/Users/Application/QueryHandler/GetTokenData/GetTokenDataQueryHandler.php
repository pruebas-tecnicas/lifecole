<?php

declare(strict_types=1);

namespace Lifecole\Main\UsersContext\Users\Application\QueryHandler\GetTokenData;

use Exception;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\ValidationData;
use Lifecole\Main\SharedContext\Application\Message;
use Lifecole\Main\SharedContext\Application\QueryBus\QueryHandler;

class GetTokenDataQueryHandler extends QueryHandler
{
    /**
     * @param Message|GetTokenDataQuery $query
     *
     * @return GetTokenDataQueryReply
     * @throws Exception
     */
    public function __invoke(Message $query): GetTokenDataQueryReply
    {
        $parser = new Parser();
        $token = $parser->parse($query->token());

        $data = new ValidationData();
        $data->setIssuer('www.demo.wip');
        $data->setAudience('www.demo.wip');
        $data->setId('07091966sz');

        if (!$token->validate($data)) {
            throw new Exception('Invalid token');
        }

        return GetTokenDataQueryReply::create($token);
    }
}
