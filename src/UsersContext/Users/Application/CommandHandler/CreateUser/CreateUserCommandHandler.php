<?php

declare(strict_types=1);

namespace Lifecole\Main\UsersContext\Users\Application\CommandHandler\CreateUser;

use Lifecole\Main\SharedContext\Application\CommandBus\CommandHandler;
use Lifecole\Main\SharedContext\Application\Message;
use Lifecole\Main\SharedContext\Domain\Exception\UserExistsException;
use Lifecole\Main\SharedContext\Domain\Identifier\UserId;
use Lifecole\Main\UsersContext\Users\Domain\Entity\User;
use Lifecole\Main\UsersContext\Users\Domain\Entity\UserReadRepositoryInterface;
use Lifecole\Main\UsersContext\Users\Domain\Entity\UserWriteRepositoryInterface;

final class CreateUserCommandHandler extends CommandHandler
{
    private UserReadRepositoryInterface $userReadRepository;
    private UserWriteRepositoryInterface $userWriteRepository;

    public function __construct(
        UserReadRepositoryInterface $userReadRepository,
        UserWriteRepositoryInterface $userWriteRepository
    ) {
        $this->userReadRepository  = $userReadRepository;
        $this->userWriteRepository = $userWriteRepository;
    }

    /**
     * @param Message|CreateUserCommand $command
     *
     * @throws UserExistsException
     */
    public function __invoke(Message $command): void
    {
        $user = $this->userReadRepository->findByUsername($command->username());

        if (!is_null($user)) {
            throw UserExistsException::create($command->username());
        }

        $user = User::create(UserId::new(), $command->username(), $command->secret());
        $this->userWriteRepository->save($user);
    }
}
