<?php

declare(strict_types=1);

namespace Lifecole\Main\UsersContext\Users\UserInterface\ConsoleCommand;

use Lifecole\Main\SharedContext\Application\CommandBus\CommandBus;
use Lifecole\Main\SharedContext\Domain\Exception\UserExistsException;
use Lifecole\Main\SharedContext\UserInterface\ConsoleCommand;
use Lifecole\Main\UsersContext\Users\Application\CommandHandler\CreateUser\CreateUserCommand;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Symfony5CreateUserConsoleCommand extends ConsoleCommand
{
    protected static $defaultName = 'demo:users:create';

    private CommandBus $commandBus;

    public function __construct(CommandBus $synchronousCommandBus)
    {
        parent::__construct(self::$defaultName);
        $this->commandBus = $synchronousCommandBus;
    }

    protected function configure()
    {
        $this
            ->setDescription('Create a user with pass 1234')
            ->setHelp('Create a new User but at this moment the password is always 1234')
            ->addArgument('username', InputArgument::OPTIONAL, 'The username')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $username = $input->getArgument('username');
        if (is_null($username)) {
            $this->writeText($output, 'error', 'The username is required');

            return self::FAILURE;
        }

        try {
            $this->commandBus->process(CreateUserCommand::create($username, '1234'));
        } catch (UserExistsException $exception) {
            $this->writeText($output, 'error', $exception->getMessage());

            return self::FAILURE;
        }

        $this->showOkMessage($output, $input);

        return self::SUCCESS;
    }

    protected function showOkMessage(OutputInterface $output, InputInterface $input): void
    {
        $output->getFormatter()->setStyle(
                'ok',
                new OutputFormatterStyle('black', 'green', [])
            );
        $this->writeText(
            $output,
            'ok',
            sprintf('the user "%s" has been created', $input->getArgument('username'))
        );
    }

    private function writeText(OutputInterface $output, string $type, string $text): void
    {
        $chars = mb_strlen($text) + 4;
        $output->writeln('');
        $output->writeln("<$type>" . str_pad('', $chars, ' ') . "</$type>");
        $output->writeln("<$type>  $text  </$type>");
        $output->writeln("<$type>" . str_pad('', $chars, ' ') . "</$type>");
        $output->writeln('');
    }
}
