<?php

declare(strict_types=1);

namespace Lifecole\Main\UsersContext\Users\UserInterface\ConsoleCommand;

use Lifecole\Main\SharedContext\Application\CommandBus\CommandBus;
use Lifecole\Main\SharedContext\Application\QueryBus\QueryBus;
use Lifecole\Main\SharedContext\UserInterface\ConsoleCommand;
use Lifecole\Main\UsersContext\Users\Application\QueryHandler\GetUserToken\GetUserTokenQuery;
use Lifecole\Main\UsersContext\Users\Application\QueryHandler\GetUserToken\GetUserTokenQueryReply;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;

class Symfony5ShowUserTokenConsoleCommand extends ConsoleCommand
{
    protected static $defaultName = 'demo:users:token';

    private QueryBus $queryBus;

    public function __construct(QueryBus $queryBus)
    {
        parent::__construct(self::$defaultName);
        $this->queryBus = $queryBus;
    }

    protected function configure()
    {
        $this
            ->setDescription('Show the user and creates a token with 1 hour validity')
            ->setHelp('Show the user and creates a token with 1 hour validity')
            ->addArgument('username', InputArgument::OPTIONAL, 'The username')
            ->addArgument('password', InputArgument::OPTIONAL, 'The password')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $username = $input->getArgument('username');
        if (is_null($username)) {
            $this->writeText($output, 'error', 'The username is required');

            return self::FAILURE;
        }

        $password = $input->getArgument('password');
        if (is_null($password)) {
            $this->writeText($output, 'error', 'The password is required');

            return self::FAILURE;
        }

        try {
            /** @var GetUserTokenQueryReply $result */
            $result = $this->queryBus->askFor(GetUserTokenQuery::create($username, $password));
        } catch (Throwable $exception) {
            $this->writeText($output, 'error', $exception->getMessage());

            return self::FAILURE;
        }

        $this->showOkMessage($output);
        $this->writeText($output, 'info', $result->getToken()->__toString());

        return self::SUCCESS;
    }

    protected function showOkMessage(OutputInterface $output): void
    {
        $output->getFormatter()->setStyle(
                'ok',
                new OutputFormatterStyle('black', 'green', [])
            );
        $this->writeText(
            $output,
            'ok',
            'Token generated'
        );
    }

    private function writeText(OutputInterface $output, string $type, string $text): void
    {
        $chars = mb_strlen($text) + 4;
        $output->writeln('');
        $output->writeln("<$type>" . str_pad('', $chars, ' ') . "</$type>");
        $output->writeln("<$type>  $text  </$type>");
        $output->writeln("<$type>" . str_pad('', $chars, ' ') . "</$type>");
        $output->writeln('');
    }
}
