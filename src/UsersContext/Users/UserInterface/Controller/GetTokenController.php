<?php

declare(strict_types=1);

namespace Lifecole\Main\UsersContext\Users\UserInterface\Controller;

use Lifecole\Main\SharedContext\Domain\Exception\InvalidArgumentException;
use Lifecole\Main\SharedContext\Domain\Exception\UserNotFoundException;
use Lifecole\Main\SharedContext\UserInterface\Controller;
use Lifecole\Main\UsersContext\Users\Application\QueryHandler\GetUserToken\GetUserTokenQuery;
use Lifecole\Main\UsersContext\Users\Application\QueryHandler\GetUserToken\GetUserTokenQueryReply;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class GetTokenController extends Controller
{
    public function __invoke(Request $request): JsonResponse
    {
        try {
            $query = GetUserTokenQuery::create(
                $request->request->get('username', ''),
                $request->request->get('password', '')
            );

            /** @var GetUserTokenQueryReply $reply */
            $reply = $this->queryBus->askFor($query);
        } catch (InvalidArgumentException $exception) {
            return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
        } catch (UserNotFoundException $exception) {
            return new JsonResponse(null, Response::HTTP_UNAUTHORIZED);
        }

        return new JsonResponse(['token' => $reply->getToken()->__toString()], Response::HTTP_OK);
    }
}
