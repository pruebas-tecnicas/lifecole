<?php

declare(strict_types=1);

namespace Lifecole\Main\UsersContext\Users\Infrastructure\Repository;

use Lifecole\Main\SharedContext\Domain\Exception\InvalidArgumentException;
use Lifecole\Main\SharedContext\Infrastructure\Persistence\DatabaseInterface;
use Lifecole\Main\UsersContext\Users\Domain\Entity\User;
use Lifecole\Main\UsersContext\Users\Domain\Entity\UserReadRepositoryInterface;
use Lifecole\Main\UsersContext\Users\Domain\ValueObject\Secret;
use Lifecole\Main\UsersContext\Users\Domain\ValueObject\Username;

class PostgresUserReadRepository implements UserReadRepositoryInterface
{
    private DatabaseInterface $database;

    public function __construct(DatabaseInterface $database)
    {
        $this->database = $database;
    }

    /**
     * @param Username $username
     *
     * @return User|null
     * @throws InvalidArgumentException
     */
    public function findByUsername(Username $username): ?User
    {
        $query = 'SELECT * FROM customer WHERE username = :username';

        $statement = $this->database->query(
            $query,
            [
                ':username' => $username->value()
            ]
        );

        $result =  $statement->fetch();

        if (!$result) {
            return null;
        }

        return UserConverter::arrayToUser($result);
    }

    public function findByUsernameAndSecret(Username $username, Secret $secret): ?User
    {
        $query = 'SELECT * FROM customer WHERE username = :username AND secret = :secret';

        $statement = $this->database->query(
            $query,
            [
                ':username' => $username->value(),
                ':secret'   => $secret->value()
            ]
        );

        $result =  $statement->fetch();

        if (!$result) {
            return null;
        }

        return UserConverter::arrayToUser($result);
    }
}
