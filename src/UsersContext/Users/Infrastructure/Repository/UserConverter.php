<?php

declare(strict_types=1);

namespace Lifecole\Main\UsersContext\Users\Infrastructure\Repository;

use Lifecole\Main\SharedContext\Domain\Exception\InvalidArgumentException;
use Lifecole\Main\SharedContext\Domain\Identifier\UserId;
use Lifecole\Main\UsersContext\Users\Domain\Entity\User;
use Lifecole\Main\UsersContext\Users\Domain\ValueObject\Secret;
use Lifecole\Main\UsersContext\Users\Domain\ValueObject\Username;

final class UserConverter
{
    const ID       = 'id';
    const USERNAME = 'username';
    const SECRET   = 'secret';

    public static function userToArray(User $user): array
    {
        return [
            ':'.self::ID       => $user->id()->value(),
            ':'.self::USERNAME => $user->username()->value(),
            ':'.self::SECRET   => $user->secret()->value()
        ];
    }

    /**
     * @param array $record
     *
     * @return User
     * @throws InvalidArgumentException
     */
    public static function arrayToUser(array $record): User
    {
        return User::create(
            UserId::create($record[self::ID]),
            Username::create($record[self::USERNAME]),
            Secret::create($record[self::SECRET])
        );
    }
}
