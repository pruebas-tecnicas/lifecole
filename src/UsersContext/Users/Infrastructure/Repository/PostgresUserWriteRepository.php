<?php

declare(strict_types=1);

namespace Lifecole\Main\UsersContext\Users\Infrastructure\Repository;

use Lifecole\Main\SharedContext\Infrastructure\Persistence\DatabaseInterface;
use Lifecole\Main\UsersContext\Users\Domain\Entity\User;
use Lifecole\Main\UsersContext\Users\Domain\Entity\UserWriteRepositoryInterface;

class PostgresUserWriteRepository implements UserWriteRepositoryInterface
{
    private const SAVE = 'INSERT INTO customer(id, username, secret) VALUES (:id, :username, :secret);';

    private DatabaseInterface $database;

    public function __construct(DatabaseInterface $database)
    {
        $this->database = $database;
    }

    public function save(User $user): void
    {
        $events = $user->takeEvents();
        if (count($events) === 0) {
            return;
        }

        $this->database->exec(
            self::SAVE,
            UserConverter::userToArray($user)
        );
    }
}
