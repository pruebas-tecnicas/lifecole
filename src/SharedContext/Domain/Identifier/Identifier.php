<?php

declare(strict_types=1);

namespace Lifecole\Main\SharedContext\Domain\Identifier;

use Lifecole\Main\SharedContext\Domain\Exception\InvalidArgumentException;

abstract class Identifier
{
    const IDENTIFIER_PATTERN = '/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/';

    protected string $value;

    private function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * @param string $value
     *
     * @return $this
     * @throws InvalidArgumentException
     */
    final public static function create(string $value): self
    {
        if (preg_match(self::IDENTIFIER_PATTERN, $value) !== 1) {
            throw InvalidArgumentException::create(__METHOD__, $value);
        }
        return new static($value);
    }

    public static function new(): self
    {
        return self::generateFromRandomBytes();
    }

    final function value(): string
    {
        return $this->value;
    }

    final public function equals(self $other): bool
    {
        return $this->value() === $other->value();
    }

    protected static function generateFromRandomBytes(): self
    {
        $translate = '0123456789abcdef';
        $bytes = random_bytes(20);

        $uuid='';
        for ($a=2; $a<18; $a++) {
            $ord = ord(substr($bytes, $a, 1));
            if ($a===8) {
                $ord = ($ord | 0x40) & 0x4f;
            }
            if ($a===10) {
                $ord = ($ord | 0x80) & 0xbf;
            }
            $uuid .= substr($translate, ($ord & 0xf0) >> 4, 1) . substr($translate, $ord & 0x0f, 1);
        }
        return new static(preg_replace('/(.{8})(.{4})(.{4})(.{4})(.{12})/', '\\1-\\2-\\3-\\4-\\5', $uuid));
    }
}
