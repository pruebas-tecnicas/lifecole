<?php

declare(strict_types=1);

namespace Lifecole\Main\SharedContext\Domain\Entity;

use Lifecole\Main\SharedContext\Domain\Identifier\Identifier;

abstract class Entity
{
    protected Identifier $id;

    protected function __construct(Identifier $id)
    {
        $this->id = $id;
    }

    final public function id(): Identifier
    {
        return $this->id;
    }
}
