<?php

declare(strict_types=1);

namespace Lifecole\Main\SharedContext\Domain\Entity;

use Lifecole\Main\SharedContext\Domain\Event\DomainEvent;
use Lifecole\Main\SharedContext\Domain\Event\DomainEvents;
use Lifecole\Main\SharedContext\Domain\Exception\InvalidArgumentException;
use Lifecole\Main\SharedContext\Domain\Identifier\Identifier;

abstract class AggregateRoot extends Entity
{
    private array $events;

    protected function __construct(Identifier $id)
    {
        parent::__construct($id);

        $this->events = [];
    }

    final public function recordEvent(DomainEvent ...$events): void
    {
        foreach ($events as $event) {
            $this->events[] = $event;
        }
    }

    /**
     * @return DomainEvents
     * @throws InvalidArgumentException
     */
    final public function takeEvents(): DomainEvents
    {
        $events = DomainEvents::create(...$this->events);

        $this->events = [];

        return $events;
    }
}
