<?php

declare(strict_types=1);

namespace Lifecole\Main\SharedContext\Domain\ValueObject;

use ArrayAccess;
use Countable;
use Iterator;
use Lifecole\Main\SharedContext\Domain\Exception\InvalidArgumentException;

abstract class Collection implements ArrayAccess, Iterator, Countable
{
    private array $rows = [];
    private int   $idx  = 0;

    /**
     * @param array $initialObjects
     *
     * @throws InvalidArgumentException
     */
    final public function __construct(array $initialObjects = [])
    {
        foreach ($initialObjects as $object) {
            $this->offsetSet(null, $object);
        }
    }

    abstract protected function classOfArrayElements(): string;
    /**
     * @param mixed $offset
     * @param mixed $value
     *
     * @throws InvalidArgumentException
     */
    final public function offsetSet($offset, $value)
    {
        $classOfArrayElements = $this->classOfArrayElements();
        if (get_class($value) instanceof $classOfArrayElements) {
            throw InvalidArgumentException::create(__METHOD__, $value);
        }

        if ($offset === null) {
            $this->rows[] = $value;

            return;
        }

        $this->rows[$offset] = $value;
    }

    final public function offsetExists($offset)
    {
        return isset($this->rows[$offset]);
    }

    final public function offsetUnset($offset)
    {
        unset($this->rows[$offset]);
    }

    final public function offsetGet($offset)
    {
        return $this->rows[$offset];
    }

    final public function rewind()
    {
        $this->idx = 0;
    }

    final public function valid()
    {
        return $this->idx < count($this->rows);
    }

    final public function current()
    {
        return $this->rows[$this->idx];
    }

    final public function key()
    {
        return $this->idx;
    }

    final public function next()
    {
        $this->idx++;
    }

    final public function count()
    {
        return count($this->rows);
    }
}
