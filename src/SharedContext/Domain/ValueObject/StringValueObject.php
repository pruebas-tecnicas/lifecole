<?php

declare(strict_types=1);

namespace Lifecole\Main\SharedContext\Domain\ValueObject;

use Lifecole\Main\SharedContext\Domain\Exception\InvalidArgumentException;

abstract class StringValueObject
{
    private string $value;

    /**
     * @param string $value
     *
     * @throws InvalidArgumentException
     */
    final private function __construct(string $value)
    {
        if (!$this->isAValidValue($value)) {
            throw InvalidArgumentException::create(static::class, $value);
        }

        $this->value = $value;
    }

    /**
     * @param string $value
     *
     * @return static
     * @throws InvalidArgumentException
     */
    final public static function create(string $value): self
    {
        return new static($value);
    }

    final function value(): string
    {
        return $this->value;
    }

    final public function equals(self $other): bool
    {
        return $this->value() === $other->value();
    }

    abstract public function isAValidValue(string $value): bool;
}
