<?php

declare(strict_types=1);

namespace Lifecole\Main\SharedContext\Domain\Event;

use Lifecole\Main\SharedContext\Domain\Exception\InvalidArgumentException;
use Lifecole\Main\SharedContext\Domain\ValueObject\Collection;

final class DomainEvents extends Collection
{
    /**
     * @param DomainEvent ...$values
     *
     * @return static
     * @throws InvalidArgumentException
     */
    public static function create(DomainEvent ...$values): self
    {
        return new static($values);
    }

    protected function classOfArrayElements(): string
    {
        return DomainEvent::class;
    }
}
