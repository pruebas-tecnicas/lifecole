<?php

declare(strict_types=1);

namespace Lifecole\Main\SharedContext\Domain\Event;

use DateTimeImmutable;
use Lifecole\Main\SharedContext\Domain\Identifier\DomainEventId;
use Lifecole\Main\SharedContext\Domain\Identifier\Identifier;

abstract class DomainEvent
{
    public const EVENT_TIME_FORMAT = 'Y-m-d\TH:i:s.uP';

    private DomainEventId $id;
    private string        $aggregateRootId;
    private array         $payload;
    private int           $version;
    private string        $occurredOn;

    final protected function __construct(
        string $aggregateRootId,
        array $payload,
        int $version
    ) {
        $this->id              = DomainEventId::new();
        $this->aggregateRootId = $aggregateRootId;
        $this->payload         = $payload;
        $this->version         = $version;
        $this->occurredOn      = (new DateTimeImmutable())->format(self::EVENT_TIME_FORMAT);
    }

    public function id(): DomainEventId
    {
        return $this->id;
    }

    abstract function aggregateRootId(): Identifier;

    public function getPayload(): array
    {
        return $this->payload;
    }

    public function version(): int
    {
        return $this->version;
    }

    public function occurredOn(): DateTimeImmutable
    {
        return DateTimeImmutable::createFromFormat(self::EVENT_TIME_FORMAT, $this->occurredOn);
    }

    protected function aggregateRootIdString(): string
    {
        return $this->aggregateRootId;
    }
}
