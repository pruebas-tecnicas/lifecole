<?php

declare(strict_types=1);

namespace Lifecole\Main\SharedContext\Domain\Exception;

use Exception;
use Throwable;

class InvalidContentException extends Exception
{
    private function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public static function create(): self
    {
        return new static('The file content format is invalid');
    }
}
