<?php

declare(strict_types=1);

namespace Lifecole\Main\SharedContext\Domain\Exception;

use Exception;
use Lifecole\Main\SharedContext\Domain\Identifier\CarId;
use Throwable;

class CarNotFoundException extends Exception
{
    private function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public static function byId(CarId $id): self
    {
        return new static(sprintf('Car with identifier (%s) does not exist', $id->value()));
    }
}
