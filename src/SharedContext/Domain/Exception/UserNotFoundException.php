<?php

declare(strict_types=1);

namespace Lifecole\Main\SharedContext\Domain\Exception;

use Exception;
use Lifecole\Main\UsersContext\Users\Domain\ValueObject\Username;
use Throwable;

class UserNotFoundException extends Exception
{
    private function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public static function create(Username $username): self
    {
        return new static(sprintf('User with username (%s) does not exist', $username->value()));
    }
}
