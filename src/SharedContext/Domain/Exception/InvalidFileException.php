<?php

declare(strict_types=1);

namespace Lifecole\Main\SharedContext\Domain\Exception;

use Exception;
use Throwable;

class InvalidFileException extends Exception
{
    private function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public static function create(string $value): self
    {
        return new static(sprintf('The file (%s) not exists or is not readable', $value));
    }
}
