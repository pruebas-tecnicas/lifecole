<?php

declare(strict_types=1);

namespace Lifecole\Main\SharedContext\Domain\Exception;

use Exception;
use Throwable;

class InvalidArgumentException extends Exception
{
    private function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public static function create(string $methodName, $value): self
    {
        return new static(
            sprintf(
                'Invalid argument (%s) is not valid in the call to method (%s)',
                (string) $value,
                $methodName
            )
        );
    }
}
