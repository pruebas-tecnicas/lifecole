<?php

declare(strict_types=1);

namespace Lifecole\Main\SharedContext\Domain\Exception;

use Exception;
use Throwable;

class InvalidParameterException extends Exception
{
    private function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public static function create(string $parameterName, string $parameterValue, ?string $possibleValues = null): self
    {
        if (is_null($possibleValues)) {
            return new static(
                sprintf(
                    'Invalid value (%s) for parameter (%s)',
                    $parameterValue,
                    $parameterName
                )
            );
        }

        return new static(
            sprintf(
                'Invalid value (%s) for parameter (%s), valid values %s.',
                $parameterValue,
                $parameterName,
                $possibleValues
            )
        );
    }
}
