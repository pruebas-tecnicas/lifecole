<?php

declare(strict_types=1);

namespace Lifecole\Main\SharedContext\Domain\Exception;

use Exception;
use Lifecole\Main\UsersContext\Users\Domain\ValueObject\Username;
use Throwable;

class UserExistsException extends Exception
{
    private function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public static function create(Username $username): self
    {
        return new static(sprintf('A user with username (%s) already exists', $username->value()));
    }
}
