<?php
declare(strict_types=1);

namespace Lifecole\Main\SharedContext\Infrastructure\Persistence;

use PDOStatement;

interface DatabaseInterface
{
    public function beginTransaction(): bool;

    public function commit(): bool;

    public function rollBack(): bool;

    public function inTransaction(): bool;

    public function exec(string $statement, array $parameters = []): int;

    public function query (string $statement, array $parameters): PDOStatement;
}
