<?php

declare(strict_types=1);

namespace Lifecole\Main\SharedContext\Infrastructure\Persistence;

use Lifecole\Main\SharedContext\Infrastructure\Persistence\Connection\ConnectionInterface;
use PDO;
use PDOStatement;

class Database implements DatabaseInterface
{
    private ConnectionInterface $connection;

    public function __construct(ConnectionInterface $connection)
    {
        $this->connection = $connection;
    }

    public function beginTransaction(): bool
    {
        return $this->connection->connect()->beginTransaction();
    }

    public function commit(): bool
    {
        return $this->connection->connect()->commit();
    }

    public function rollBack(): bool
    {
        return $this->connection->connect()->rollBack();
    }

    public function inTransaction(): bool
    {
        return $this->connection->connect()->inTransaction();
    }

    public function exec(string $statement, array $parameters = []): int
    {
        $statement = $this->quoteAndReplace($statement, $parameters);

        $result = $this->connection->connect()->exec($statement);

        return (int)$result;
    }

    public function query(string $statement, array $parameters): PDOStatement
    {
        $statement = $this->quoteAndReplace($statement, $parameters);

        return $this->connection->connect()->query($statement, PDO::FETCH_ASSOC);
    }

    private function quoteAndReplace(string $statement, array $parameters): string
    {
        foreach ($parameters as $key => $parameter) {
            if (is_string($parameter)) {
                $parameters[$key] = $this->connection->connect()->quote($parameter);
            }
            if (is_bool($parameter)) {
                $parameters[$key] = $parameter ? 'true' : 'false';
            }
            $statement = preg_replace(
                '/' . preg_quote($key) . '/',
                $parameters[$key],
                $statement
            );
        }

        return $statement;
    }
}
