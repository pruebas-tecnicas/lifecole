<?php

declare(strict_types=1);

namespace Lifecole\Main\SharedContext\Infrastructure\Persistence\Connection;

use PDO;

class PostgresConnection implements ConnectionInterface
{
    private string $host;
    private string $port;
    private string $dbname;
    private string $user;
    private string $password;
    private string $dsn;
    private PDO    $pdo;

    final public function __construct(string $host, string $port, string $dbname, string $user, string $password)
    {
        $this->host = $host;
        $this->port = $port;
        $this->dbname = $dbname;
        $this->user = $user;
        $this->password = $password;

        $this->dsn = "pgsql:host=$host;port=$port;dbname=$dbname;user=$user;password=$password";
        $this->pdo = new PDO($this->dsn);
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function connect(): PDO
    {
        return $this->pdo;
    }

    private function __clone() {
    }

    private function __wakeup() {
    }
}
