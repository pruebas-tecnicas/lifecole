<?php
declare(strict_types=1);

namespace Lifecole\Main\SharedContext\Infrastructure\Persistence\Connection;

use PDO;

interface ConnectionInterface
{
    public function connect(): PDO;
}
