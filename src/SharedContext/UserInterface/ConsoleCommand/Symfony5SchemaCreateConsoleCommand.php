<?php

declare(strict_types=1);

namespace Lifecole\Main\SharedContext\UserInterface\ConsoleCommand;

use Lifecole\Main\SharedContext\Infrastructure\Persistence\DatabaseInterface;
use Lifecole\Main\SharedContext\UserInterface\ConsoleCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Symfony5SchemaCreateConsoleCommand extends ConsoleCommand
{
    protected static $defaultName = 'demo:schema:create';

    private DatabaseInterface $database;

    public function __construct(DatabaseInterface $database)
    {
        parent::__construct(self::$defaultName);
        $this->database = $database;
    }

    protected function configure()
    {
        $this
            ->setDescription('Create the Database schema')
            ->setHelp('Create the Database schema')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $path = realpath(__DIR__ . '/../../../../resources');
        $statement = file_get_contents($path . '/migrations.sql');
        $this->database->exec($statement);
        return self::SUCCESS;
    }
}
