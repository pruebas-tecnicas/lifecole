<?php

declare(strict_types=1);

namespace Lifecole\Main\SharedContext\UserInterface;

use Symfony\Component\Console\Command\Command;

abstract class ConsoleCommand extends Command
{
}
