<?php

declare(strict_types=1);

namespace Lifecole\Main\SharedContext\UserInterface;

use Lifecole\Main\SharedContext\Application\QueryBus\QueryBus;
use Lifecole\Main\UsersContext\Users\Application\QueryHandler\GetTokenData\GetTokenDataQuery;
use Lifecole\Main\UsersContext\Users\Application\QueryHandler\GetUserToken\GetUserTokenQueryReply;
use Symfony\Component\HttpFoundation\Request;

abstract class Controller
{
    protected QueryBus $queryBus;

    public function __construct(QueryBus $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    protected function extractUsernameFromToken(Request $request): array
    {
        $authorization = $request->headers->get('Authorization');
        $textToken = preg_replace('/^(Bearer )(.*)$/', '\\2', $authorization);
        $query = GetTokenDataQuery::create($textToken);

        /** @var GetUserTokenQueryReply $reply */
        $reply = $this->queryBus->askFor($query);

        return [
            'user_id'  => $reply->getToken()->getClaim('user_id'),
            'username' => $reply->getToken()->getClaim('username')
        ];
    }
}
