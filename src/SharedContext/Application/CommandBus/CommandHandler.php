<?php

declare(strict_types=1);

namespace Lifecole\Main\SharedContext\Application\CommandBus;

use Lifecole\Main\SharedContext\Application\Message;

abstract class CommandHandler
{
    abstract public function __invoke(Message $command): void;
}
