<?php

declare(strict_types=1);

namespace Lifecole\Main\SharedContext\Application\CommandBus;

use DateInterval;
use DateTimeImmutable;
use Lifecole\Main\SharedContext\Application\Exception\HandlerNotFoundException;
use Lifecole\Main\SharedContext\Application\Message;
use Lifecole\Main\SharedContext\Application\Middleware\Middleware;
use Lifecole\Main\SharedContext\Domain\Event\DomainEvent;
use Lifecole\Main\SharedContext\Domain\Exception\InvalidArgumentException;
use Psr\Log\LoggerInterface;

final class LoggerCommandMiddleware extends Middleware
{
    private LoggerInterface $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param Middleware|null $middleware
     *
     * @throws InvalidArgumentException
     */
    public function setNextMiddleware(?Middleware $middleware): void
    {
        if (is_null($middleware)) {
            throw InvalidArgumentException::create(
                __METHOD__,
                __CLASS__ . ' shouldn\'t be the latest Middleware'
            );
        }

        $this->next = $middleware;
    }

    /**
     * @param Message $command
     *
     * @throws HandlerNotFoundException
     */
    public function __invoke(Message $command): void
    {
        $start                   = new DateTimeImmutable();
        $memory_assigned         = memory_get_usage(false);
        $memory_usage            = memory_get_usage(false);
        $maximum_memory_assigned = memory_get_peak_usage(true);
        $maximum_memory_usage    = memory_get_peak_usage(false);

        $this->logger->info(
            'Message process init',
            [
                'Occurred on'             => $start->format(DomainEvent::EVENT_TIME_FORMAT),
                'Message class'           => get_class($command),
                'Elapsed time'            => '0000-00-00T00:00:00.000000+00:00',
                'memory_assigned'         => $memory_assigned,
                'memory_usage'            => $memory_usage,
                'maximum_memory_assigned' => $maximum_memory_assigned,
                'maximum_memory_usage'    => $maximum_memory_usage,
                'cpu_usage'               => sys_getloadavg()[0],
            ]
        );

        $this->next->__invoke($command);

        $end = new DateTimeImmutable();
        $end_memory_assigned = memory_get_usage(false);
        $end_memory_usage = memory_get_usage(false);
        $end_maximum_memory_assigned = memory_get_peak_usage(true);
        $end_maximum_memory_usage = memory_get_peak_usage(false);

        $diff = $start->diff($end);
        $this->logger->info(
            'Message process finish',
            [
                'Occurred on'                  => $start->format(DomainEvent::EVENT_TIME_FORMAT),
                'Message class'                => get_class($command),
                'Elapsed time'                 => $this->intervalFormat($diff),
                'end_memory_assigned'          => $end_memory_assigned,
                'end_memory_usage'             => $end_memory_usage,
                'end_maximum_memory_assigned'  => $end_maximum_memory_assigned,
                'end_maximum_memory_usage'     => $end_maximum_memory_usage,
                'diff_memory_assigned'         => $end_memory_assigned - $memory_assigned,
                'diff_memory_usage'            => $end_memory_usage - $memory_usage,
                'diff_maximum_memory_assigned' => $end_maximum_memory_assigned - $maximum_memory_assigned,
                'diff_maximum_memory_usage'    => $end_maximum_memory_usage - $maximum_memory_usage,
                'cpu_usage'                    => sys_getloadavg()[0],
            ]
        );
    }

    private function intervalFormat(DateInterval $interval): string
    {
        $seconds  = $interval->d * 24 * 60 * 60;
        $seconds += $interval->h * 60 * 60;
        $seconds += $interval->i * 60;
        $seconds += $interval->s;
        $result = (string) $seconds;

        $microseconds = substr((string) $interval->f, 2);

        $result .= '.' . str_pad($microseconds, 6, '0', STR_PAD_RIGHT);

        return $result;
    }
}
