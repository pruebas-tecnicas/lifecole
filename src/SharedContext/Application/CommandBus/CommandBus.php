<?php

declare(strict_types=1);

namespace Lifecole\Main\SharedContext\Application\CommandBus;

use Lifecole\Main\SharedContext\Application\Message;
use Lifecole\Main\SharedContext\Application\Middleware\Middleware;
use Lifecole\Main\SharedContext\Domain\Exception\InvalidArgumentException;

class CommandBus
{
    private Middleware $middleware;

    /**
     * @param Middleware ...$middlewares
     *
     * @throws InvalidArgumentException
     */
    final public function __construct(Middleware ...$middlewares)
    {
        if (empty($middlewares)) {
            throw InvalidArgumentException::create(__METHOD__, 'empty middlewares');
        }

        $middleware = array_pop($middlewares);
        $middleware->setNextMiddleware(null);

        while ($previous = array_pop($middlewares)) {
            $previous->setNextMiddleware($middleware);
            $middleware = $previous;
        }

        $this->middleware = $middleware;
    }

    final public function process(Message $command): void
    {
        $this->middleware->__invoke($command);
    }
}
