<?php

declare(strict_types=1);

namespace Lifecole\Main\SharedContext\Application\CommandBus;

use Lifecole\Main\SharedContext\Application\Exception\HandlerNotFoundException;
use Lifecole\Main\SharedContext\Application\Message;
use Lifecole\Main\SharedContext\Application\Middleware\Middleware;
use Lifecole\Main\SharedContext\Domain\Exception\InvalidArgumentException;

final class ProcessCommandMiddleware extends Middleware
{
    /** @var array $handlers */
    private array $handlers = [];

    public function __construct(iterable $commandHandlers)
    {
        foreach ($commandHandlers as $commandHandler) {
            $handlerClassName = get_class($commandHandler);
            $commandClassName = preg_replace('/^(.*)Handler$/', '\\1', $handlerClassName);

            $this->handlers[$commandClassName] = $commandHandler;
        }
    }

    /**
     * @param Middleware|null $middleware
     *
     * @throws InvalidArgumentException
     */
    public function setNextMiddleware(?Middleware $middleware): void
    {
        if (!is_null($middleware)) {
            throw InvalidArgumentException::create(
                __METHOD__,
                'ProcessCommandMiddleware should be the latest Middleware'
            );
        }
    }

    /**
     * @param Message $command
     *
     * @throws HandlerNotFoundException
     */
    public function __invoke(Message $command): void
    {
        if (!isset($this->handlers[get_class($command)])) {
            throw HandlerNotFoundException::create(get_class($command));
        }

        $handler = $this->handlers[get_class($command)];
        $handler($command);
    }
}
