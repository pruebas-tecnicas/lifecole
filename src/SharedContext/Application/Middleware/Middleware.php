<?php
declare(strict_types=1);

namespace Lifecole\Main\SharedContext\Application\Middleware;

use Lifecole\Main\SharedContext\Application\Message;

abstract class Middleware
{
    protected Middleware $next;

    abstract public function setNextMiddleware(?Middleware $middleware): void;

    abstract public function __invoke(Message $message);
}
