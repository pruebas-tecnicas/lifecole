<?php

declare(strict_types=1);

namespace Lifecole\Main\SharedContext\Application\QueryBus;

use Lifecole\Main\SharedContext\Application\Message;
use Lifecole\Main\SharedContext\Application\Middleware\Middleware;
use Lifecole\Main\SharedContext\Application\Reply;
use Lifecole\Main\SharedContext\Domain\Exception\InvalidArgumentException;

class QueryBus
{
    private Middleware $middleware;

    /**
     * @param Middleware ...$middlewares
     *
     * @throws InvalidArgumentException
     */
    final public function __construct(Middleware ...$middlewares)
    {
        if (empty($middlewares)) {
            throw InvalidArgumentException::create(__METHOD__, 'empty middlewares');
        }

        $middleware = array_pop($middlewares);
        $middleware->setNextMiddleware(null);

        while ($previous = array_pop($middlewares)) {
            $previous->setNextMiddleware($middleware);
            $middleware = $previous;
        }

        $this->middleware = $middleware;
    }

    final public function askFor(Message $query): Reply
    {
        return $this->middleware->__invoke($query);
    }
}
