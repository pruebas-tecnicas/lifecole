<?php

declare(strict_types=1);

namespace Lifecole\Main\SharedContext\Application\QueryBus;

use Lifecole\Main\SharedContext\Application\Message;
use Lifecole\Main\SharedContext\Application\Reply;

abstract class QueryHandler
{
    abstract public function __invoke(Message $query): Reply;
}
