<?php

declare(strict_types=1);

namespace Lifecole\Main\SharedContext\Application\EventBus;

use Lifecole\Main\SharedContext\Application\Message;

abstract class EventHandler
{
    // TODO: At the moment it is not possible for the implementation to declare a child class as a parameter.
    // abstract public function __invoke(Message $event): void;
}
