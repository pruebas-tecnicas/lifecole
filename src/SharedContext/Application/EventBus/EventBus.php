<?php

declare(strict_types=1);

namespace Lifecole\Main\SharedContext\Application\EventBus;

use Lifecole\Main\SharedContext\Application\Message;
use Lifecole\Main\SharedContext\Application\Middleware\Middleware;
use Lifecole\Main\SharedContext\Domain\Exception\InvalidArgumentException;

class EventBus
{
    private Middleware $middleware;

    /**
     * @param Middleware ...$middlewares
     *
     * @throws InvalidArgumentException
     */
    final public function __construct(Middleware ...$middlewares)
    {
        if (empty($middlewares)) {
            throw InvalidArgumentException::create(__METHOD__, 'empty middlewares');
        }

        $middleware = array_pop($middlewares);
        $middleware->setNextMiddleware(null);

        while ($previous = array_pop($middlewares)) {
            $previous->setNextMiddleware($middleware);
            $middleware = $previous;
        }

        $this->middleware = $middleware;
    }

    final public function publish(Message $event): void
    {
        $this->middleware->__invoke($event);
    }
}
