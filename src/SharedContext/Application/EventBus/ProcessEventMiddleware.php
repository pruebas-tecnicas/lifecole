<?php

declare(strict_types=1);

namespace Lifecole\Main\SharedContext\Application\EventBus;

use Lifecole\Main\SharedContext\Application\Exception\HandlerNotFoundException;
use Lifecole\Main\SharedContext\Application\Message;
use Lifecole\Main\SharedContext\Application\Middleware\Middleware;
use Lifecole\Main\SharedContext\Domain\Exception\InvalidArgumentException;

final class ProcessEventMiddleware extends Middleware
{
    /** @var array $handlers */
    private array $handlers = [];

    public function __construct(iterable $commandHandlers)
    {
        foreach ($commandHandlers as $commandHandler) {
            $handlerClassName = get_class($commandHandler);
            $commandClassName = preg_replace('/^(.*)Handler$/', '\\1', $handlerClassName);

            $this->handlers[$commandClassName] = $commandHandler;
        }
    }

    /**
     * @param Middleware|null $middleware
     *
     * @throws InvalidArgumentException
     */
    public function setNextMiddleware(?Middleware $middleware): void
    {
        if (!is_null($middleware)) {
            throw InvalidArgumentException::create(
                __METHOD__,
                'ProcessCommandMiddleware should be the latest Middleware'
            );
        }
    }

    /**
     * @param Message $event
     *
     * @throws HandlerNotFoundException
     */
    public function __invoke(Message $event): void
    {
        if (!isset($this->handlers[get_class($event)])) {
            throw HandlerNotFoundException::create(get_class($event));
        }

        $handler = $this->handlers[get_class($event)];
        $handler($event);
    }
}
