<?php

declare(strict_types=1);

namespace Lifecole\Main\SharedContext\Application\Exception;

use Exception;
use Throwable;

class HandlerNotFoundException extends Exception
{
    private function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public static function create(string $messageFullClassName): self
    {
        return new static('Handler not found for ' . $messageFullClassName);
    }
}
