# Backend Test

## What is this about 


The purpose of this test is to know your ability to create a modern and functional API environment.

Below you’ll find the features, the requirements, and the key points you should use while developing. 


## What we’ll evaluate 


You are free to use any PHP/TypeScript/Go... libraries and/or frameworks you want. However, a DDD/Hexagonal style framework is encouraged.

The use of classes, modules, bundles, and entities is allowed and encouraged.

The use of Docker, docker-compose, or Kubernetes is allowed and encouraged.

You can choose the database(s) that you think are the best options to solve the problems and explain why.

The following are bonus points for the candidate: 

    - Code quality

    - Application structure 

    - Unit testing 

    - Optimization and performance (load times)

    - Scalability

    - HTTP Codes usage

    - Exceptions handler

    - Best practices

    - SOLID principles


## What we want you to do


As a user I want to have access to a private (with authorization) Cars API where I can search items given the following criteria: 

    - Brand

    - Model

    - Price (two independent currencies available, euros and EEUU dollars)

When I perform a search that has results, I’d like to be able to get a list of the cars listing all that information. Also, I want to have the ability to order the listed items by brand, model, and price. 

Also, I want to be able to mark/unmark a car as a favorite, and I want to filter by favorite cars.

As the car list tends to infinity, should be paginated as you want. You can select the pagination strategy, it’s up to you.


## Delivery requirements 

The data source must be the data.txt file like a 3r party data provider that only changes one time each day. All the models that should be displayed are inside this file. You should decide how you want to access and treat this data thinking about performance and best practices.

The prices aren't included in the file. You should generate it with a 3rd party API. The value doesn't need to be real. You can use this service to generate random values http://www.randomnumberapi.com .

You must attach your solution with all the files needed to work in Github/Bitbucket private repository with a complete README with the setup and run instructions.


# Solución

A parte de dar las gracias por la oportunidad recibida, comentarles que no he podido llevar a cabo el 100% de los requisitos de la prueba aplicando correctamente todos mis conocimientos dado que los requisitos de la prueba y los puntos que comentan van a valorar constituyen al menos un sprint de 2 semanas a jornada completa y yo solo he tenido dos fines de semana para realizarlo.

Para la prueba he escogido:

- ***PHP*** y ***Symfony*** por que son el lenguaje de programación y el framework que más domino y el puesto es para php y symfony/laravel.

- ***docker*** y ***docker-compose*** para la infraestructura, kubernetes no lo he implementado por falta de tiempo para configurarlo y luego documentarlo, para la prueba tampoco se necesitaban levantar varias instancias del mismo container así que lo descarté.

- Para la arquitectura una variación de ***arquitectura hexagonal*** el ***CQRS*** y lo he intentado implantar aplicando ***DDD*** lo que en realidad es casi una ilusión pues sin más contexto que el par de frases de lo que se requiere y sin un lenguaje ubicuo no hay información suficiente para desarrollar la ***parte estratégica*** de este sistema de modelado pero por el contrario si he podido profundizar en la ***parte táctica***.

## Infraestructura

En la raíz del proyecto se encuentra una carpeta docker con los ficheros necesarios para levantar el container del frontal web (nginx), el de backend (php-fpm) y el de base de datos (postgresql), se ha escogido esta configuración porque en los requerimientos se expresaba la necesidad de optimización y rendimiento así como la escalabilidad.

- Docker nos permite en primer lugar trabajar en local con las mismas versiones y configuraciones que se utilizarán para poner el proyecto en producción, en producción además utilizaríamos kubernetes que simplifica el sistema de deploy y facilita las herramientas para poder levantar múltiples instancias de un mismo contenedor a fin de dar solución a picos de demanda o crecimiento orgánico.

- Se ha escogido la configuración nginx y php-fpm pues si bien apache + modulo de php es más rápido para una única instancia cuando queremos escalar debemos poner delante un balanceador incluido nginx, momento en el que la combinación balanceador+apache+modulo php pasa a ser menos eficiente que balanceador (nginx) + php-fpm.

- Se han escogido las versiones 7.4.x de PHP y 5.x de Symfony por motivos de rendimiento, PHP 7.4 posee la capacidad (una vez configurado) de pre-cargar y pre-compilar clases en memoria de forma permanente durante todo el tiempo que el servidor php-fpm esté levantado lo que en algunos benchmaks realizados en combinación con symfony 5.1 ha dado reportes de un 75% más de peticiones procesadas con el mismo código subyacente.

- Como base de datos se ha escogido Postgres porque es mucho más eficiente con el uso de uuid's, necesita menos recursos y también si fuese necesario incluye herramientas para consulta en campos json y datos geoespaciales como coordenadas geográficas.

- Me hubiese gustado poder instalar Redis para ejemplificar su uso como cache de base de datos y RabbitMq para ejemplificar el uso de colas de eventos pero con un tiempo tan limitado ha sido imposible.

## Arquitectura

El ***apartado de estrategia*** de *DDD* se ha supeditado a la creación de una estructura de ejemplo que demuestre que tengo conocimientos en ese terreno pero que como se ha comentado antes no es posible llevar a cabo sin más información.

Para ello se ha creado:

- El **Bounded Context** de *Cars* que contiene el **AggregateRoot** *Car* y el módulo *Favorites* 

- El **Bounded Context** de *Shared* que contiene los elementos genéricos comunes a todos los **Bounded Context** y **Modules** como puede ser los identificadores de las **entities**, las Excepciones, los buses, etc.

- El **Bounded Context** de *Users o Customer*

En el ***apartado táctico*** se ha optado por:

- No complicarnos con excesivos sub-niveles de carpetas por lo cual directamente en src/ encontramos los Bounded Context con su nombre más el sufijo Context 'xxxxContext'

- En el especial caso de SharedContext su contenido se divide en:

    - Application

        Contiene los *contratos* en forma de clases abstractas de los buses de comandos, consultas, eventos, middleware, etc.

    - Domain

        Contiene las bases de los AggregateRoots, Entities, Events, Exceptions, Identifiers y ValueObjects genéricos.

    - Infrastructure

        Contiene las conexiones a bases de datos y el Kernel de Symfony

    - UserInterface

        Contiene las clases abstractas que definen los contratos para los Comandos de Consola, los Consumidores de eventos y los Controladores API, además de un Comando de Consola que genera el **Schema** de la base de datos.

- El ***CarsContext*** contiene dos módulos el AggregateRoot que da nombre al ***BoundedContext*** *Cars* y el ***module*** *Favorites* y el ***BoundedContext*** *Users* que contiene el ***AggregateRoot*** *Users*
- Los ***AggregateRoots*** y los ***modules*** contienen 4 carpetas Domain, Application, Infrastructure y UserInterface.
    - Domain

        Contiene los agregados, entidades, valueObjects, eventos y servicios de dominio. La idea es que la lógica de validación de datos permanezca en valueObjects y Entities, así como cualquier lógica de mutación de estado de los agregados y las entidades, cuando esta lógica es más compleja de lo que estos elementos pueden manejar esta se delega a los servicios de dominio.
    - Application

        En una arquitectura basada en CQRS aquí encontraremos los (casos de uso) en forma de Handler de comandos, eventos y queries. 
    - Infrastructure

        Aquí encontraremos la implementación de todos aquellos contratos en forma de interfaces definidos en Domain y Application (en nuestro caso Repositorios y AntiCorruptionLayer) mencionar que lo normal es usar AntiCorruptionLayer para conectar nuestro sistema con otras partes legacy de el mismo usando el patrón "Facade" y no para usarlo con conexiones a sistemas de terceros que se haría con repositorios, pero lo he creído conveniente para poder dar esta explicación.

        Anotar que aquí es donde se implementan los "*adapters*" en ***Arquitectura Hexagonal*** o ***Ports And Adapter***.

    - UserInterface o UI

        O "*ports*" en ***Arquitectura Hexagonal***.

        Es aquí donde se encuentran todas las interfaces para que el usuario/consumer (sea humano o no) pueda acceder a nuestro sistema y proporcionar y/o consumir información del mismo.

        Los casos típicos (pero no únicos) son Los controladores HTTP para APIs o páginas Web, Los Comandos de consola para acciones realizadas desde el shell del ordenador o mediante sistemas automáticos como crons y los consumidores de eventos que suelen recoger datos en forma de mensajes desde de colas de RabbitMQ, Amazon SNS, etc.

Estructura de carpetas y fichero en el momento de redactar este documento para poder hacerse una idea de la distribución expuesta en los puntos anteriores.
```
src
├── CarsContext
│   ├── Cars
│   │   ├── Application
│   │   │   ├── CommandHandler
│   │   │   │   └── UpdateCars
│   │   │   │       ├── UpdateCarsCommand.php
│   │   │   │       └── UpdateCarsCommandHandler.php
│   │   │   ├── EventHandler
│   │   │   └── QueryHandler
│   │   │       ├── GetCar
│   │   │       │   ├── GetCarQuery.php
│   │   │       │   ├── GetCarQueryHandler.php
│   │   │       │   └── GetCarQueryReply.php
│   │   │       └── GetCars
│   │   │           ├── GetCarsQuery.php
│   │   │           ├── GetCarsQueryHandler.php
│   │   │           └── GetCarsQueryReply.php
│   │   ├── Domain
│   │   │   ├── Entity
│   │   │   │   ├── Car.php
│   │   │   │   ├── CarImporterInterface.php
│   │   │   │   ├── CarReadRepositoryInterface.php
│   │   │   │   ├── CarWriteRepositoryInterface.php
│   │   │   │   ├── Cars.php
│   │   │   │   └── PriceImporterInterface.php
│   │   │   ├── Event
│   │   │   │   ├── CarCreatedEvent.php
│   │   │   │   └── CarFieldUpdatedEvent.php
│   │   │   ├── Service
│   │   │   └── ValueObject
│   │   │       ├── Amount.php
│   │   │       ├── Currency.php
│   │   │       ├── Enabled.php
│   │   │       ├── ExternalId.php
│   │   │       ├── FullModelName.php
│   │   │       ├── Price.php
│   │   │       ├── Schema.php
│   │   │       ├── ShortModelName.php
│   │   │       └── UpdatedAt.php
│   │   ├── Infrastructure
│   │   │   ├── AntiCorruptionLayer
│   │   │   │   ├── CarTSVImporter.php
│   │   │   │   ├── PriceImporter.php
│   │   │   │   └── TSVIterator.php
│   │   │   └── Repository
│   │   │       ├── CarConverter.php
│   │   │       ├── PostgresCarReadRepository.php
│   │   │       └── PostgresCarWriteRepository.php
│   │   └── UserInterface
│   │       ├── ConsoleCommand
│   │       │   ├── Symfony5CheckCarsImportFileConsoleCommand.php
│   │       │   └── Symfony5ImportCarsConsoleCommand.php
│   │       ├── Consumer
│   │       └── Controller
│   │           ├── GetCarController.php
│   │           └── GetCarsController.php
│   └── Favorites
│       ├── Application
│       │   ├── CommandHandler
│       │   ├── EventHandler
│       │   └── QueryHandler
│       ├── Domain
│       │   ├── Entity
│       │   │   ├── Favourite.php
│       │   │   ├── FavouriteReadRepositoryInterface.php
│       │   │   └── FavouriteWriteRepositoryInterface.php
│       │   ├── Event
│       │   ├── Service
│       │   └── ValueObject
│       ├── Infrastructure
│       │   ├── AntiCorruptionLayer
│       │   └── Repository
│       │       ├── PostgresFavouriteReadRepository.php
│       │       └── PostgresFavouriteWriteRepository.php
│       └── UserInterface
│           ├── ConsoleCommand
│           ├── Consumer
│           └── Controller
│               └── PostFavouriteController.php
├── SharedContext
│   ├── Application
│   │   ├── CommandBus
│   │   │   ├── CommandBus.php
│   │   │   ├── CommandHandler.php
│   │   │   ├── LoggerCommandMiddleware.php
│   │   │   └── ProcessCommandMiddleware.php
│   │   ├── EventBus
│   │   │   ├── EventBus.php
│   │   │   ├── EventHandler.php
│   │   │   ├── LoggerEventMiddleware.php
│   │   │   └── ProcessEventMiddleware.php
│   │   ├── Exception
│   │   │   └── HandlerNotFoundException.php
│   │   ├── Message.php
│   │   ├── Middleware
│   │   │   └── Middleware.php
│   │   ├── QueryBus
│   │   │   ├── LoggerQueryMiddleware.php
│   │   │   ├── ProcessQueryMiddleware.php
│   │   │   ├── QueryBus.php
│   │   │   └── QueryHandler.php
│   │   └── Reply.php
│   ├── Domain
│   │   ├── Entity
│   │   │   ├── AggregateRoot.php
│   │   │   └── Entity.php
│   │   ├── Event
│   │   │   ├── DomainEvent.php
│   │   │   └── DomainEvents.php
│   │   ├── Exception
│   │   │   ├── CarNotFoundException.php
│   │   │   ├── InvalidArgumentException.php
│   │   │   ├── InvalidContentException.php
│   │   │   ├── InvalidFileException.php
│   │   │   ├── InvalidParameterException.php
│   │   │   ├── UserExistsException.php
│   │   │   └── UserNotFoundException.php
│   │   ├── Identifier
│   │   │   ├── CarId.php
│   │   │   ├── DomainEventId.php
│   │   │   ├── FavouriteId.php
│   │   │   ├── Identifier.php
│   │   │   ├── PriceId.php
│   │   │   └── UserId.php
│   │   └── ValueObject
│   │       ├── Collection.php
│   │       └── StringValueObject.php
│   ├── Infrastructure
│   │   ├── Persistence
│   │   │   ├── Connection
│   │   │   │   ├── ConnectionInterface.php
│   │   │   │   └── PostgresConnection.php
│   │   │   ├── Database.php
│   │   │   └── DatabaseInterface.php
│   │   └── Symfony
│   │       └── Kernel.php
│   └── UserInterface
│       ├── ConsoleCommand
│       │   └── Symfony5SchemaCreateConsoleCommand.php
│       ├── ConsoleCommand.php
│       ├── Consumer.php
│       └── Controller.php
└── UsersContext
    └── Users
        ├── Application
        │   ├── CommandHandler
        │   │   └── CreateUser
        │   │       ├── CreateUserCommand.php
        │   │       └── CreateUserCommandHandler.php
        │   ├── EventHandler
        │   └── QueryHandler
        │       ├── GetTokenData
        │       │   ├── GetTokenDataQuery.php
        │       │   ├── GetTokenDataQueryHandler.php
        │       │   └── GetTokenDataQueryReply.php
        │       └── GetUserToken
        │           ├── GetUserTokenQuery.php
        │           ├── GetUserTokenQueryHandler.php
        │           └── GetUserTokenQueryReply.php
        ├── Domain
        │   ├── Entity
        │   │   ├── User.php
        │   │   ├── UserReadRepositoryInterface.php
        │   │   └── UserWriteRepositoryInterface.php
        │   ├── Event
        │   │   └── UserCreatedEvent.php
        │   ├── Service
        │   └── ValueObject
        │       ├── Secret.php
        │       └── Username.php
        ├── Infrastructure
        │   ├── AntiCorruptionLayer
        │   └── Repository
        │       ├── PostgresUserReadRepository.php
        │       ├── PostgresUserWriteRepository.php
        │       └── UserConverter.php
        └── UserInterface
            ├── ConsoleCommand
            │   ├── Symfony5CreateUserConsoleCommand.php
            │   └── Symfony5ShowUserTokenConsoleCommand.php
            ├── Consumer
            └── Controller
                └── GetTokenController.php
```

## Optimización, Rendimiento

El primer punto abordado ha sido la versión de PHP y Symfony que se ha explicado en el apartado de Infraestructura.

El segundo punto ha sido la instalación de la versión mínima (para APIs) de Symfony

Aunque el uso de librerías es una de las mejores practicas (no reinventar la rueda) en el caso de querer una gran optimización y rendimiento no vemos forzados a implementar nosotros mismos algunas de estas librerías que al ser de uso genérico incorporan un gran conjunto de clases y configuraciones que no se adaptan o no necesitamos e incrementan en mucho el tiempo de deploy y el tiempo de respuesta a cada petición al tener que levantar toda esa configuración no necesaria.

- En vez de utilizar Symfony/Messenger o TheLeague/Tactician como librería para el uso de buses (ddd/cqrs) he implementado una versión 100% operativa que se encuentra en *SharedContext/Application* y que incorpora todas las necesidades de buses de comandos, de eventos, de queries tanto síncronos como asíncronos si bien es verdad que la funcionalidad asíncrona necesita de un sistema de colas que no se ha incluido, también contiene como no podía ser de otra forma del uso de Middlewares y un sistema sencillo de ampliación de estos middlewares cuando las necesidades lo requieran. La configuración de todo el sistema de buses+middlewares lo podemos encontrar en el principio del fichero config/services.yaml
```
services:
    _defaults:
        autowire: true
        autoconfigure: true
        public: false
        bind:
            $synchronousCommandBus:  '@synchronous.command.bus'
            $asynchronousCommandBus: '@asynchronous.command.bus'
            $synchronousEventBus:    '@synchronous.event.bus'
            $asynchronousEventBus:   '@asynchronous.event.bus'
            $queryBus:               '@query.bus'

    _instanceof:
        Lifecole\Main\SharedContext\Application\CommandBus\CommandHandler:
            tags:
                - 'command.handler'

        Lifecole\Main\SharedContext\Application\EventBus\EventHandler:
            tags:
                - 'event.handler'

        Lifecole\Main\SharedContext\Application\QueryBus\QueryHandler:
            tags:
                - 'query.handler'

        Lifecole\Main\SharedContext\UserInterface\ConsoleCommand:
            tags:
                - 'console.command'

        Lifecole\Main\SharedContext\UserInterface\Consumer:
            tags:
                - 'lifecole.consumer'

        Lifecole\Main\SharedContext\UserInterface\Controller:
            tags:
                - 'controller.service_arguments'


    # default configuration
    Lifecole\Main\:
        resource: '../src/'


    # Bus configuration
    asynchronous.command.bus:
        class: Lifecole\Main\SharedContext\Application\CommandBus\CommandBus
        arguments:
            - '@Lifecole\Main\SharedContext\Application\CommandBus\LoggerCommandMiddleware'
            - '@Lifecole\Main\SharedContext\Application\CommandBus\ProcessCommandMiddleware'
        tags:
            - { name: 'container.preload', class: 'Lifecole\Main\SharedContext\Application\CommandBus\CommandBus' }

    synchronous.command.bus:
        class: Lifecole\Main\SharedContext\Application\CommandBus\CommandBus
        arguments:
            - '@Lifecole\Main\SharedContext\Application\CommandBus\LoggerCommandMiddleware'
            - '@Lifecole\Main\SharedContext\Application\CommandBus\ProcessCommandMiddleware'
        tags:
            - { name: 'container.preload', class: 'Lifecole\Main\SharedContext\Application\CommandBus\CommandBus' }

    asynchronous.event.bus:
        class: Lifecole\Main\SharedContext\Application\EventBus\EventBus
        arguments:
            - '@Lifecole\Main\SharedContext\Application\EventBus\LoggerEventMiddleware'
            - '@Lifecole\Main\SharedContext\Application\EventBus\ProcessEventMiddleware'
        tags:
            - { name: 'container.preload', class: 'Lifecole\Main\SharedContext\Application\EventBus\EventBus' }

    synchronous.event.bus:
        class: Lifecole\Main\SharedContext\Application\EventBus\EventBus
        arguments:
            - '@Lifecole\Main\SharedContext\Application\EventBus\LoggerEventMiddleware'
            - '@Lifecole\Main\SharedContext\Application\EventBus\ProcessEventMiddleware'
        tags:
            - { name: 'container.preload', class: 'Lifecole\Main\SharedContext\Application\EventBus\EventBus' }

    query.bus:
        class: Lifecole\Main\SharedContext\Application\QueryBus\QueryBus
        arguments:
            - '@Lifecole\Main\SharedContext\Application\QueryBus\LoggerQueryMiddleware'
            - '@Lifecole\Main\SharedContext\Application\QueryBus\ProcessQueryMiddleware'
        tags:
            - { name: 'container.preload', class: 'Lifecole\Main\SharedContext\Application\QueryBus\QueryBus' }


    # Middlewares
    Lifecole\Main\SharedContext\Application\CommandBus\ProcessCommandMiddleware:
        - !tagged_iterator command.handler

    Lifecole\Main\SharedContext\Application\QueryBus\ProcessQueryMiddleware:
        - !tagged_iterator query.handler
```
- También he optado por crear un sistema rudimentario pero eficaz de acceso y gestión de la base de datos, Symfony nos puede proveer del ORM Doctrine, pero es complejo de manejar y configurar si no se conoce y es muy lento por la gran cantidad de funcionalidades que provee de las que al menos en este mini proyecto no vamos a utilizar. Los ficheros son pocos y sencillos, los encontraremos en ***SharedContext/Infrastructure/Persistence***, ***SharedContext/UserInterface/ConsoleCommand/Symfony5SchemaCreateConsoleCommand.php*** y su mínima configuración en ***config/services.yaml***.


## Instalación


(proyectos) Ruta a la carpeta donde tienes tus proyectos

(demo) Carpeta dentro de (proyectos) donde está o estará instalado este proyecto

Ejecutar en el shell de su sistema (no está probado en windows)
```
    cd (proyectos)
    git clone https://bitbucket.org/pruebas-tecnicas/lifecode.git (demo)
    cd (demo)
    make install
    make start
```

Los comandos anteriores habrán creado una carpeta para el proyecto, descargado el código desde internet, generado las imágenes docker del proyecto, instalado las dependencias de composer en el container de php y creado el schema de base de datos.


## Funcionamiento


Estamos listos para probar la funcionalidad, a continuación se muestran los comandos de consola para probar toda la funcionalidad, pero en las partes en la que se refiere a peticiones curl se puede utilizar postman para tal cometido en (demo)/postman se puede importar la colección y environment demo.

primero comprobaremos como conseguir un token (pero sin un usuario creado) lo que no nos dará realmente el token:

```
curl --location --request POST 'http://localhost:80/authorization' --header 'Content-Type: application/x-www-form-urlencoded' --data-urlencode 'username=pepe' --data-urlencode 'password=1234'
```

ahora crearemos un usuario, se puede ejecutar el comando de symfony desde el container o usar el comando de make creado para abreviar, los dos ejecutan finalmente el mismo comando de symfony:

```
make create-user USERNAME=pepe
```

o

```
docker-compose exec php bin/console demo:users:create pepe
```

A tener en cuenta, por simplicidad al crear un usuario siempre se hace con la contraseña "1234" y en base de datos esta contraseña se almacena encriptada bajo la denominación "secret".

Volvamos a pedir un token, tenemos dos posibilidades, creadas para demostrar la reutilización de código, el generar un token para un usuario que proporcione su username y password se ha realizado en el ***Bounded Context*** de *Users* en *Application/QueryHandler/GetUserToken* y se ha utilizado en dos **ports** uno como comando de consola src/UsersContext/Users/UserInterface/ConsoleCommand/Symfony5CreateUserConsoleCommand.php y otro com endpoint web en src/UsersContext/Users/UserInterface/Controller/GetTokenController.php así que los dos nos proporcionarán un token JWT válido por 5 minutos, si en cualquier momento en una petición a la API recibimos como respuesta ```{"error_message":"Invalid token"}``` deberemos volver a generar un token nuevo para usarlo con las siguientes peticiones.

```
curl --location --request POST 'http://localhost:80/authorization' --header 'Content-Type: application/x-www-form-urlencoded' --data-urlencode 'username=pepe' --data-urlencode 'password=1234'
```

o

```
make create-token USERNAME=pepe PASSWORD=1234
```


Ahora vamos a pedir un listado de coches usando el token que acabamos de generar, debe sustituir en el ejemplo de abajo (token) por el token generado en el punto anterior:

```
curl --location --request GET 'http://localhost:80/cars' --header 'Authorization: Bearer (token)'
```

La respuesta será un array JSON vacío pues no hemos cargado la lista de coches desde nuestro proveedor, hemos creado un fichero de datos adicional para mostrar la funcionalidad de la importación y el mantenimiento de los favoritos cuando un coche deja de estar disponible y vuelve a estarlo más tarde.

Primero cargaremos la lista completa de coches, usaré el comando make para simplificar, pero se puede consultar en Makefile el comando real que se ejecuta en el container php.

```
make import-original-file
```

Este proceso tardará 4 o 5 minutos en terminar, pues aunque se ha optimizado la infraestructura y el código para un rendimiento óptimo los requisitos de consultar los precios a una web externa hace que se tengan que realizar 2 peticiones de precio para cada uno de los 468 coches del listado, 936 peticiones en total.

Indicar que para un acercamiento a un proceso de este tipo en la realidad la carga se ha realizado línea a línea con una petición de precio independiente para Euros y Dólares pues aunque el proceso se podría haber realizado de golpe reduciendo los tiempos de carga muchísimo, en la realidad estos 468 coches podrían ser miles y dejarían al sistema sin recursos y la operación no se realizaría nunca.

Volvamos a pedir la lista de coches (puede que tenga que volver a generar un token pues el anterior posiblemente esté caducado):

```
curl --location --request GET 'http://localhost:80/cars' --header 'Authorization: Bearer (token)'
```

Ahora si, obtendremos una lista de 25 coches con todos sus datos, al no estar filtrada por nada en los resultados entran todos los coches y solo se muestran los 25 primeros, si queremos ver más deberemos añadir el parámetro *page* indicando la página de resultados a mostrar:

```
curl --location --request GET 'http://localhost:80/cars?page=2' --header 'Authorization: Bearer (token)'
```

El resto de opciones de consulta que se pedían en los requerimientos se muestran en la siguiente petición:

```
curl --location --request GET 'http://localhost:80/cars?filter_field=(www)&filter_value=(xxx)&order_field=(yyy)&favourite=(zzz)&page=2' --header 'Authorization: Bearer (token)'
```

Esta consulta tiene algunas restricciones:

- filter_field y filter_value no pueden aparecer la una sin la otra es decir o se usan las dos o ninguna

- (www) solo puede contener los valores: brand, model, price.

- (xxx) debe contener:

    - SEAT, VOLKSWAGEN, AUDI, etc para filter_field=brand
    
    - ALTEA, AROSA, A3, A4, etc para filter_field=model
    
    - un número entero para filter_field=price que filtrará precios en EUR o USD menores a esa cantidad.

- (zzz) solo admite el valor null o true

Si cogemos uno de los ids de coche, podremos hacer la petición para visualizar únicamente el contenido de este:

```
curl --location --request GET 'http://localhost:80/car/(car_id)' --header 'Authorization: Bearer (token)'
```

Con ese mismo id podemos crear un "favorito" con petición siguiente, en este caso el id de usuario se extrae del token JWT, es decir el favorito se crea para el usuario actual:

```
curl --location --request POST 'http://localhost:80/favourite' --data-urlencode 'car_id=(car_id)' --header 'Authorization: Bearer (token)' --header 'Content-Type: application/x-www-form-urlencoded'
```

Por último si ahora (pasadas 24 horas) volvemos a cargar la lista de coches, pero lo hacemos con el fichero alternativo que solo contiene los datos de los vehículos BMW veremos que los comandos para listar coches solo nos muestran esta marca, sin embargo en la base de datos permanecen todos los vehículos restantes desactivados lo que nos brinda la oportunidad de mantener los favoritos por usuario y que estos estén disponibles si se vuelve a cargar la lista completa de coches.

```
make import-alternate-file
```

Cuando terminemos de usar el proyecto solo tenemos que parar los containers:

```
make stop
```


## Conclusiones


Aclaraciones sobre los puntos a desarrollar:

- Se ha creado la configuración de las imágenes de docker necesarias para el proyecto.

- Se ha creado un fichero Makefile para ofrecer ayuda y un método sencillo de ejecutar comandos de uso habitual

- Se ha creado en Makefile una "rule" para la instalación y puesta en funcionamiento del proyecto.

- Se ha creado un sistema de "buses" (de commands, queries y events) sencillo, eficiente y optimizado.

- Se ha creado un sistema de gestión de la base de datos super simplificado pero completamente eficiente y  optimizado.

- Se ha creado el proceso de carga/actualización de los ficheros TSV como un *comando de consola* que procesa unitáriamente las líneas de dicho ficheros para que en el caso de crecer el número de líneas no exceda de los recursos de la máquina, se ha realizado con un *caso de uso* (CommandHandler) por lo cual realizar el cambio para leer los datos de un fichero con otro formato o incluso peticionar los datos a un endpoint sería relativamente sencillo, también nos facilita si lo deseamos leer el fichero de forma rápida encolando los comandos de creación haciendo que se ejecuten de forma asíncrona.

- Adicionalmente se ha creado un comando para validar el formato de los ficheros TSV ```make check-original-file``` y ```make check-alternate-file``` básicamente realiza el mismo proceso de importación, pero muestra los datos por pantalla en vez de almacenarlos en base de datos.

- Se ha creado el endpoint para generación de tokens

- Se ha creado el endpoint para consulta del listado de coches con filtros, ordenación y paginación.

- Se ha creado el endpoint para la consulta de un coche especifico a través de su identificador.

- Se ha creado el endpoint para marcar un coche como favorito

- Se ha creado este fichero como documentación que una vez terminado contendrá las 70 líneas de su documento de requisitos más unas 570-600 líneas de explicaciones, gráficos y comandos de documentación.

Aclaraciones sobre los puntos a valorar:

- La calidad del código ha sido observada

    - Se han observado los patrones SOLID
        
        - SRP: Cada clase o método solo hace una cosa
        
        - OCP: Se ha realizado de forma que sea fácil añadir funcionalidad sin tener que modificar el código existente.
        
        - LSP: Se ha observado el principio de Liskov y eso se puede observar por ejemplo en la herencia de los Conmmand, Query y Event handlers.
        
        - ISP: Este principio también se ha tenido en cuenta y por esa razón las interfaces de los repositorios han sido divididas en 2, la interfaz que contiene los métodos de lectura y la que contiene los métodos de escritura. Esto también viene dado por el uso de CQRS.

        - DI: Todas las instanciaciones de clases susceptibles de cambio son inyectadas en los constructores y solo los valueObjects, Entities y algún servicio de dominio sin estado se ha instanciado estáticamente.
        
    - Se ha observado YAGNI, KISS y DRY

- La estructura de la aplicación tal vez no sea la óptima y me refiero con esto a que no se ha podido profundizar en el concepto de (estrategia) del DDD, pues no había suficiente información. Pero si se ha tenido en cuenta el concepto (táctico) de DDD y la se paración por capas de la Arquitectura Hexagonal así como la separación de las operaciones de lectura y escritura del CQRS.

- La optimización y rendimiento ya se ha explicado en puntos anteriores y si bien la carga de datos es lenta por la cantidad ingente de peticiones HTTP al exterior algunos ejemplos del rendimiento pueden ser:

    - La petición para generar un nuevo token tarda entre 206 y 222 milisegundos y esto teniendo en cuenta que php está compilado con xdebug para el desarrollo y funcionando en docker en un macbook que utiliza una máquina virtual lo que lo hace lento.
    
    - La petición del listado de coches con todos los filtros activados consume entre 274 y 322 milisegundos cuando devuelve una página con 25 elementos y entre 205 y 262 milisegundos cuando la página contiene un único elemento.
    
    - La petición de los datos de un único coche consume 202 y 226 milisegundos
    
- La escalabilidad en nuestro caso solo depende de kubernetes (que no se ha implementado) aunque si la gestión de coches y usuarios se convirtiesen en cargas pesadas el hecho de estar en bounded context separados nos da facilidad para separalos en microservicios distintos que pueden escalar independientemente.

- Todas las repuestas de las APIs desarrolladas devuelven los códigos HTTP correspondientes y el contenido si lo hay en formato JSON

- Los distintos elementos de Domain y Application lanzan excepciones controladas cuando se encuentran en situaciones no válidas para su correcto funcionamiento y son los "ports" (Contrladores, Comandos de consola) los que capturan esas excepciones y determinan la respuesta y el formato más correcto para el usuario.

- Best practices y SOLID principles creo que ya está explicados en los puntos anteriores

- Unit testing, este es el único punto que no he podido cumplir y sinceramente por falta de tiempo, una vez enviado el enlace a este proyecto intentaré añadir algunos tests después de terminar mi jornada laboral, pero no garantizo nada.






Ha sido un placer

Atentamente

Sergio Zambrano Delfa
