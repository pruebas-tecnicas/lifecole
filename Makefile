.ONESHELL:
.DEFAULT_GOAL := help
SHELL := /bin/bash

.PHONY: help
help:
	function make_help() { echo -e "\033[34m\033[1m$$1\033[35m  -> \033[32m$$2\033[0m"; shift; shift; for i in "$$@"; do echo -e "                          \033[36m$$i\033[0m"; done }
	make_help "help                 "  "Show this help"
	make_help "build                "  "Build or rebuild the images for the docker containers of this project"
	make_help "install              "  "Prepares the environment for the demo execution"
	make_help "start                "  "Run the project containers"
	make_help "stop                 "  "Stop the execution of the containers"
	make_help "create-user          "  "Create a user with only the username, the password is always 1234" "example:" "make create-user USERNAME=john"
	make_help "create-token         "  "Creates and show a token for a user with one hour validity" "example:" "make create-token USERNAME=john PASSWORD=1234"
	make_help "check-original-file  "  "Show the content of the original file or throw an error"
	make_help "check-alternate-file "  "Show the content of the alternate file or throw an error"
	make_help "import-original-file "  "Import the content of the original file or throw an error"
	make_help "import-alternate-file"  "Import the content of the alternate file or throw an error"

.PHONY: build
build:
	docker ps     | grep -E 'demo_(www|php|db)' | awk '{print $1}' | xargs docker stop > /dev/null
	docker ps -a  | grep -E 'demo_(www|php|db)' | awk '{print $1}' | xargs docker rm   > /dev/null
	docker images | grep -E 'demo_(www|php|db)' | awk '{print $1}' | xargs docker rmi  > /dev/null
	docker build -t demo_db:latest docker/postgres
	docker build -t demo_php:latest docker/php
	docker build -t demo_www:latest docker/nginx

.PHONY: install
install: build
	docker-compose up -d
	docker-compose exec php install
	docker-compose exec php bin/console demo:schema:create
	docker-compose stop

.PHONY: start
start:
	docker-compose up -d

.PHONY: stop
stop:
	docker-compose stop

.PHONY: create-user
create-user:
	docker-compose exec php bin/console demo:users:create ${USERNAME}

.PHONY: create-token
create-token:
	docker-compose exec php bin/console demo:users:token ${USERNAME} ${PASSWORD}

.PHONY: check-original-file
check-original-file:
	docker-compose exec php bin/console demo:cars:checkfile

.PHONY: check-alternate-file
check-alternate-file:
	docker-compose exec php bin/console demo:cars:checkfile resources/data2.txt

.PHONY: import-original-file
import-original-file:
	docker-compose exec php bin/console demo:cars:import

.PHONY: import-alternate-file
import-alternate-file:
	docker-compose exec php bin/console demo:cars:import resources/data2.txt

.PHONY: console
console:
	docker-compose exec php bin/console debug:router
