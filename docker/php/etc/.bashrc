alias l='ls -al --color'
alias rm='rm -i'
alias cp='cp -i'

NORMAL="[0m"
BLUE="[1;34m"
GREEN="[1;32m"
PS1="$GREEN\u$NORMAL@$BLUE\h $GREEN[$NORMAL\w$GREEN]$NORMAL\$ "

export PS1
export LESS="-R"
export GREP_OPTIONS="--color"
