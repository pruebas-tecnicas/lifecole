#!/bin/bash

extDir="$(php -n -d 'display_errors=stderr' -r 'echo ini_get("extension_dir");')"
files=(/usr/local/etc/php/conf.d/*.ini)
total=${#files[*]}

REGEXPART='\1'
if [ "$APP_ENV" == "prod" ] || [ "$APP_ENV" == "production" ]; then
  REGEXPART='\2'
fi

for (( i=0; i<$((total)); i++ )); do
    sed -i -e "s@#extensions#@$extDir@g" -e "s/(dev:\(.*\)\/\(.*\))/$REGEXPART/g" "${files[$i]}"
done

chmod 0755 $extDir/*.so

