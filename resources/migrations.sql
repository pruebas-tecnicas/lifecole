drop table if exists "car";

create table "car"
(
	id uuid not null
		constraint car_pk
			primary key,
	schema_id varchar(6),
	external_id varchar(6),
	short_model_name varchar(28),
	full_model_name varchar(33),
	price1_amount double precision,
	price1_currency varchar(3),
	price2_amount double precision,
	price2_currency varchar(3),
	enabled boolean,
	updated_at timestamp with time zone
);

alter table "car" owner to admin_user;

create index car_updated_at_enabled_index
	on car (updated_at, enabled);

create unique index car_external_id_uindex
	on car (external_id);

drop table if exists "user";

create table "customer"
(
	id uuid not null
		constraint user_pk
			primary key,
	username varchar(20),
	secret varchar(32)
);

alter table "customer" owner to admin_user;

create table favourite
(
	id uuid not null
		constraint favourite_pk
			primary key,
	customer_id uuid,
	car_id uuid
);

alter table favourite owner to admin_user;

create unique index favourite_customer_id_car_id_uindex
	on favourite (customer_id, car_id);

